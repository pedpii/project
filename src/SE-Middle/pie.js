import React, { Component } from 'react';
import Highcharts from 'highcharts';
import drilldown from 'highcharts-drilldown';
import { findDOMNode } from 'react-dom';

class Highchart extends Component {
    componentDidMount() {
        // load modules
        drilldown(Highcharts);

        this.chart = new Highcharts['Chart'](
            findDOMNode(this),
            this.props.options
        );
    }

    componentWillUnmount() {
        this.chart.destroy();
    }

    render() {
        return (
            <div className="in-highchart"></div>
        )
    }
}

class Piechart extends Component {

    render() {
        const options = {

            chart: {
                type: 'pie'
            },

            title: {
                text: 'กราฟแสดงจำนวนผู้ได้รับการรับรองมาตรฐานของแต่ละเครือข่าย'
            },
            accessibility: {
                announceNewData: {
                    enabled: true
                },
                point: {
                    valueSuffix: '%'
                }
            },

            plotOptions: {
                // series: {
                //     dataLabels: {
                //         enabled: true,
                //         format: '{point.name}: {point.y:.1f}%'
                //     }
                // }
                pie: {
                    showInLegend: true,
                    innerSize: "60%",
                    dataLabels: {
                      enabled: false,
                      distance: -14,
                      color: "white",
                      style: {
                        fontweight: "bold",
                        fontsize: 50
                      }
                    }
                  }
                
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
            },

            series: [
                {
                    name: "ผู้ได้รับการรับรองมาตรฐาน",
                    colorByPoint: true,
                    data: [
                        {
                            name: "Neo firm วิสาหกิจชุมชนปลาบู่ข้าวอินทรีย์",
                            y: 1.0,
                            drilldown: "Neo firm วิสาหกิจชุมชนปลาบู่ข้าวอินทรีย์"
                        },
                        {
                            name: "Neo firm เครือข่ายเกษตรกรอินทรีย์อิสาน",
                            y: 4.0,
                            drilldown: "Neo firm เครือข่ายเกษตรกรอินทรีย์อิสาน"
                        },
                        {
                            name: "Neo firm บริษัทเก้าศิริจำกัด",
                            y: 4.0,
                            drilldown: "Neo firm บริษัทเก้าศิริจำกัด"
                        },
                        {
                            name: "Neo firm บริษัทกรีนโกรทออร์แกนิค",
                            y: 0.0,
                            drilldown: "Neo firm บริษัทกรีนโกรทออร์แกนิค"
                        },
                        {
                            name: "Neo firm ภาคีเครือข่ายสุดยอดข้าวไทย",
                            y: 0.0,
                            drilldown: "Neo firm ภาคีเครือข่ายสุดยอดข้าวไทย"
                        },
                        
                        
                    ]
                }
            ],
            drilldown: {
                series: [
                    {
                        name: "มาตรฐานที่ผ่านการรับรอง",
                        id: "Neo firm วิสาหกิจชุมชนปลาบู่ข้าวอินทรีย์",
                        data: [
                            [
                                "EU",
                                0.1
                            ],
                            [
                                "GAP",
                                1.3
                            ],
                            [
                                "NOP-COR/USDA",
                                53.02
                            ],
                            [
                                "Qrgainc Thailand",
                                1.4
                            ],
                            [
                                "Fair Trade",
                                0.88
                            ],
                            [
                                "IFOAM",
                                0.56
                            ],
                            [
                                "อื่นๆ",
                                0.45
                            ]
                        ]
                    },
                    {
                        name: "มาตรฐานที่ผ่านการรับรอง",
                        id: "Neo firm เครือข่ายเกษตรกรอินทรีย์อิสาน",
                        data: [
                            [
                                "EU",
                                0.1
                            ],
                            [
                                "GAP",
                                1.3
                            ],
                            [
                                "NOP-COR/USDA",
                                53.02
                            ],
                            [
                                "Qrgainc Thailand",
                                1.4
                            ],
                            [
                                "Fair Trade",
                                0.88
                            ],
                            [
                                "IFOAM",
                                0.56
                            ],
                            [
                                "อื่นๆ",
                                0.45
                            ]
                        ]
                    },
                    {
                        name: "มาตรฐานที่ผ่านการรับรอง",
                        id: "Neo firm บริษัทเก้าศิริจำกัด",
                        data: [
                            [
                                "EU",
                                0.1
                            ],
                            [
                                "GAP",
                                1.3
                            ],
                            [
                                "NOP-COR/USDA",
                                53.02
                            ],
                            [
                                "Qrgainc Thailand",
                                1.4
                            ],
                            [
                                "Fair Trade",
                                0.88
                            ],
                            [
                                "IFOAM",
                                0.56
                            ],
                            [
                                "อื่นๆ",
                                0.45
                            ]
                        ]
                    },
                    {
                        name: "มาตรฐานที่ผ่านการรับรอง",
                        id: "Neo firm บริษัทกรีนโกรทออร์แกนิค",
                        data: [
                            [
                                "EU",
                                0.1
                            ],
                            [
                                "GAP",
                                1.3
                            ],
                            [
                                "NOP-COR/USDA",
                                53.02
                            ],
                            [
                                "Qrgainc Thailand",
                                1.4
                            ],
                            [
                                "Fair Trade",
                                0.88
                            ],
                            [
                                "IFOAM",
                                0.56
                            ],
                            [
                                "อื่นๆ",
                                0.45
                            ]
                        ]
                    },
                    {
                        name: "มาตรฐานที่ผ่านการรับรอง",
                        id: "Neo firm ภาคีเครือข่ายสุดยอดข้าวไทย",
                        data: [
                            [
                                "EU",
                                0.1
                            ],
                            [
                                "GAP",
                                1.3
                            ],
                            [
                                "NOP-COR/USDA",
                                53.02
                            ],
                            [
                                "Qrgainc Thailand",
                                1.4
                            ],
                            [
                                "Fair Trade",
                                0.88
                            ],
                            [
                                "IFOAM",
                                0.56
                            ],
                            [
                                "อื่นๆ",
                                0.45
                            ]
                        ]
                    }
                    
                ]
            }
        }
        return (
            <div>
                <Highchart highcharts={Highcharts} options={options} />
            </div>
        );
    }
}

export default Piechart;