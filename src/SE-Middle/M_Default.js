//ตระกร้าสินค้า
import React, { Component } from 'react';
import { user_token, addComma, user_token_decoded, sortData } from '../Support/Constance';
import { get, post, ip } from '../Support/Service';
import moment from 'moment'
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import arrow from '../Image/up-arrow.png'
import Pagination from "../Support/Pagination";
import M_Farmer from './M_Farmer';
import Modal from 'react-responsive-modal';
import Table from 'react-bootstrap/Table'


class M_Default extends Component {

    constructor(props) {
        super(props)
        this.state = {
            currentPage: 1,
            todosPerPage: 7,
            currentPage_unpass: 1,
            todosPerPage_unpass: 7,
            get_farmer: [],
            get_farmer_pass: [],
            get_farmer_unpass: [],
            volume_farmer: [],
            click: false,
            sumEach: [],
            sum_area_storage: [],
            name_se: [],
            index: 30,
            volume_fermer: [],
            people_pass: 0,
            people_unpass: 0,
            people_0years: 0,
            people_1years: 0,
            people_2years: 0,
            people_3years: 0,
            people_4years: 0,
            open: false,
            open1: false,
            detail_certified: [],
            detail_certified2: [],
            detail_plant_best: [],
            detail_plant_best_first: null,
            detail_plant_best_second: null,
            detail_plant_best_third: null,
            detail_plant_best_fourth: null,
            detail_plant_best_fifth: null,
            get_origin: [],

        }
    }

    // onOpenModal = () => {
    //     this.setState({ open: true });
    // };

    onCloseModal = () => {
        this.setState({ open: false });
        this.setState({ open1: false });
    };

    certified_check = (farmer_id) => {
        let index = this.state.get_farmer.findIndex((element) => element.farmer_id === farmer_id)
        let data = this.state.get_farmer[index].certified.certified_array
        let data2 = this.state.get_farmer[index].certified
        this.setState({
            open: true,
            detail_certified: data,
            detail_certified2: data2,
        })
        // console.log('detail_certified : ', data2) 
    }

    plant_type_best_show = (farmer_id) => {
        // let data = this.state.get_farmer[index].plant_type_best
        let index = this.state.get_farmer.findIndex((element) => element.farmer_id === farmer_id)
        let data1 = this.state.get_farmer[index].plant_type_best.wellplanted_first
        let data2 = this.state.get_farmer[index].plant_type_best.wellplanted_second
        let data3 = this.state.get_farmer[index].plant_type_best.wellplanted_third
        let data4 = this.state.get_farmer[index].plant_type_best.wellplanted_fourth
        let data5 = this.state.get_farmer[index].plant_type_best.wellplanted_fifth

        this.setState({
            open1: true,
            // detail_plant_best: data,
            detail_plant_best_first: data1,
            detail_plant_best_second: data2,
            detail_plant_best_third: data3,
            detail_plant_best_fourth: data4,
            detail_plant_best_fifth: data5
        })
        // console.log('plant_type_best_show : ', data)
    }

    componentWillMount() {
        this.get_name_se_all()
        this.get_Cert_post()
        this.get_volume_fermer()
    }

    sum_volume = (count_farmer) => {
        let sum = 0;
        count_farmer.map((element) => {
            return (
                sum += (element.count_farmer)
            )

        })
        return sum;

    }

    get_volume_fermer = async () => {
        try {
            await get('neutrally/get_count_se_all', user_token).then((result) => {
                if (result.success) {
                    this.setState({ volume_fermer: result.result })
                    // console.log('get_volume_fermer', result.result)
                }
                else {
                    alert(result.error_message)
                }
            })

        } catch (error) {
            alert('get_volume_fermer: ' + error)
        }
    }

    get_name_se_all = async () => {
        try {
            await get('neutrally/get_name_se_all', user_token).then((result) => {
                if (result.success) {
                    this.setState({ name_se: result.result })
                    console.log('get_name_se_all', result.result)
                }
                else {
                    alert(result.error_message)
                }
            })

        } catch (error) {
            alert('get_name_se_all: ' + error)
        }
    }

    get_Cert = (e) => {
        this.setState({
            index: e.target.value
        })
        setTimeout(() => this.get_Cert_post())
    }

    get_Cert_post = async () => {
        let index = this.state.index
        let obj = {
            user_id: index
        }
        try {
            await post(obj, 'neutrally/get_Certified_farmer_se', user_token).then((result) => {
                if (result.success) {
                    this.setState({
                        get_farmer: result.result,
                        get_origin: result.result,
                        currentPage: 1,
                    })
                    this.sum()

                    this.filter_area_storage(result.result)


                    console.log('get_fa', result.result)
                }
                else {
                    alert(result.error_message)
                }
            })
        }
        catch (error) {

        }
    }

    filter_area_storage = (data_array) => {
        let data = data_array // this.state.get_origin
        let area_storage = []
        let area_storage2 = []
        data.map((element, index) => {

            let cer_index = element.certified.certified_array.findIndex((e) => e.certified === "ไม่ผ่านการรับรองหรือรับรองตัวเอง")

            if (cer_index >= 0 || element.certified.certified_array.length === 0) {
                area_storage.push(element)
            }
            else {
                area_storage2.push(element)
            }
        })


        this.setState({
            get_farmer_unpass: area_storage,
            get_farmer_pass: area_storage2
        })

    }

    sum = () => {
        let farmer = this.state.get_origin
        let num0 = 0, num1 = 0, num2 = 0, num3 = 0, num4 = 0, num5 = 0, num6 = 0
        let area_storage = []
        farmer.map((element) => {
            let chemical = parseInt(moment().utc(7).add('years', 543).diff(moment(element.chemical_date), 'year', true))
            if (chemical === 0) {
                num0 += 1
            }
            else if (chemical === 1) {
                num1 += 1
            }
            else if (chemical === 2) {
                num2 += 1
            }
            else if (chemical === 3) {
                num3 += 1
            }
            else {
                num4 += 1
            }
            // console.log("area_element" ,element)
            if (element.area_storage !== "null") {

                area_storage.push(element)
                if (area_storage !== "null") {
                    num5 += 1
                }
                num6 = (num0 + num1 + num2 + num3 + num4) - num5
                this.state.people_pass = num5
                this.state.people_unpass = num6

            }
            this.state.people_0years = num0
            this.state.people_1years = num1
            this.state.people_2years = num2
            this.state.people_3years = num3
            this.state.people_4years = num4
        })
        this.setState({
            sumEach: [
                { name: 'น้อยกว่า 1 ปี', y: num0 },
                { name: '1 ปี', y: num1 },
                { name: '2 ปี', y: num2 },
                { name: '3 ปี', y: num3 },
                { name: 'มากกว่า 3 ปี', y: num4 }
            ],
            sum_area_storage: [
                { name: 'ผ่าน', y: num5 },
                { name: 'ไม่ผ่าน', y: num6 }
            ]
            // sum_area_storage: area_storage
        })

    }

    sort_date = () => {
        let data = this.state.get_farmer
        data.map((element) => {
            element.chemical_date = moment(element.chemical_date).format('YYYYMMDD')
        })
        sortData(data, 'chemical_date', this.state.click)
        data.map((element) => {
            element.chemical_date = moment(element.chemical_date).format('DD-MM-YYYY')
        })
        this.setState({ get_farmer: data })
        this.setState(({ click }) => ({ click: !click }))
    }

    filter_chemical_date = (e) => {
        let num = e.target.value
        // console.log(num)
        let data = this.state.get_origin
        let chemical_date = []
        if (num === "all") {
            this.setState({
                get_farmer: this.state.get_origin,
                currentPage: 1,
            })
        }
        else {
            data.map((element) => {

                let chemical = parseInt(moment().utc(7).add('years', 543).diff(moment(element.chemical_date), 'year', true))

                switch (num) {
                    case "0":
                    case "1":
                    case "2":
                    case "3":
                        if (num == chemical) {
                            chemical_date.push(
                                element
                            )
                        }
                        break;
                    case "4":
                        if (chemical >= 4) {
                            chemical_date.push(
                                element
                            )
                        }
                        break;
                    default:
                        break;
                }


            })

            this.setState({
                get_farmer: chemical_date,
                currentPage: 1,
            });
        }

    }

    changeCurrentPage = (numPage, name) => {
        this.setState({ [name]: numPage });
        // this.setState({ currentPage_unpass: numPage });
        //fetch a data
        //or update a query to get data
    };

    render() {

        const { open } = this.state;

        let todos = []
        let todos_unpass = []
        let todos_pass = []
        const { get_farmer, get_farmer_unpass, get_farmer_pass, currentPage, todosPerPage, currentPage_unpass, todosPerPage_unpass } = this.state;
        get_farmer.map((element, index) => {
            // console.log(index+1)
            todos.push({
                num: index + 1,
                ...element
            })
        })

        get_farmer_unpass.map((element, index) => {
            // console.log(index+1)
            todos_unpass.push({
                num: index + 1,
                ...element
            })
        })

        get_farmer_pass.map((element, index) => {
            // console.log(index+1)
            todos_pass.push({
                num: index + 1,
                ...element
            })
        })

        const indexOfLastTodo = currentPage * todosPerPage;
        const indexOfFirstTodo = indexOfLastTodo - todosPerPage;
        // const currentTodos = todos.slice(indexOfFirstTodo, indexOfLastTodo);
        const currentTodos_pass = todos_pass.slice(indexOfFirstTodo, indexOfLastTodo);


        const indexOfLastTodo_unpass = currentPage_unpass * todosPerPage_unpass;
        const indexOfFirstTodo_unpass = indexOfLastTodo_unpass - todosPerPage_unpass;
        const currentTodos_unpass = todos_unpass.slice(indexOfFirstTodo_unpass, indexOfLastTodo_unpass);


        console.log("currentTodos : ", currentTodos_unpass)

        //วงกลม
        const options3 = {
            chart: {
                type: 'pie'
            },
            title: {
                text: ''
            },
            plotOptions: {
                pie: {
                    showInLegend: true,
                    innerSize: "60%",
                    dataLabels: {
                        enabled: false,
                        distance: -14,
                        color: "white",
                        style: {
                            fontweight: "bold",
                            fontsize: 50
                        }
                    }
                }
            },
            series: [
                {
                    name: "ผู้ได้รับการรับรองมาตรฐาน",
                    colorByPoint: true,
                    data: [
                        {
                            name: "Neo firm วิสาหกิจชุมชนปลาบู่ข้าวอินทรีย์",
                            y: 1,
                            // drilldown: "Neo firm วิสาหกิจชุมชนปลาบู่ข้าวอินทรีย์"
                        },
                        {
                            name: "Neo firm เครือข่ายเกษตรกรอินทรีย์อิสาน",
                            y: 4,
                            // drilldown: "Neo firm เครือข่ายเกษตรกรอินทรีย์อิสาน"
                        },
                        {
                            name: "Neo firm บริษัทเก้าศิริจำกัด",
                            y: 4,
                            // drilldown: "Neo firm บริษัทเก้าศิริจำกัด"
                        },
                        {
                            name: "Neo firm บริษัทกรีนโกรทออร์แกนิค",
                            y: 0,
                            // drilldown: "Neo firm บริษัทกรีนโกรทออร์แกนิค"
                        },
                        {
                            name: "Neo firm ภาคีเครือข่ายสุดยอดข้าวไทย",
                            y: 0,
                            // drilldown: "Neo firm ภาคีเครือข่ายสุดยอดข้าวไทย"
                        },


                    ]
                }
            ]
        }

        //ผ่านหรือไม่ผ่าน
        const options2 = {

            chart: {
                renderTo: 'container',
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: 'กราฟแสดงการรับรองมาตรฐานในเครือ',
                style: {
                    fontSize: '24px',
                    fontFamily: 'fc_lamoonregular'
                }
            },
            plotOptions: {
                pie: {
                    showInLegend: true,
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                    }
                }
            },
            xAxis: {
                categories: ['ผ่าน', 'ไม่ผ่าน'],
                title: {
                    style: {
                        fontSize: '20px',
                        fontFamily: 'fc_lamoonregular'
                    }
                }
            },
            yAxis: {
                title: {
                    text: '<span style="font-size:20px;">จำนวน (คน)</span>',
                    style: {
                        fontSize: '20px',
                        fontFamily: 'fc_lamoonregular'
                    }
                }
            },
            credits: {
                enabled: false
            },
            series: [{
                showInLegend: false,
                colorByPoint: true,
                data: [
                    {
                        name: "ผ่าน",
                        y: this.state.get_farmer_pass.length,
                        color: '#5CFF11',
                    },
                    {
                        name: "ไม่ผ่าน",
                        y: this.state.get_farmer_unpass.length,
                        color: '#FF1E1E'
                    }
                ]
                // data: this.state.sum_area_storage

            }]
        }

        //ข้อมูลทั้งหมด
        const options = {
            chart: {
                renderTo: 'container',
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'column'
            },
            title: {
                text: 'กราฟแสดงจำนวนเกษตรกรที่ห่างจากการใช้สารเคมี',
                style: {
                    fontSize: '24px',
                    fontFamily: 'fc_lamoonregular'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                    }
                }
            },
            xAxis: {
                categories: ['น้อยกว่า 1 ปี', '1 ปี', '2 ปี', '3 ปี', 'มากกว่า 3 ปี'],
                title: {
                    text: '<span style="font-size:20px;">ระยะเวลา</span>',
                    style: {
                        fontSize: '20px',
                        fontFamily: 'fc_lamoonregular'
                    }
                }
            },
            yAxis: {
                // type: 'logarithmic',
                // minorTickInterval: 10
                title: {
                    text: '<span style="font-size:20px;">จำนวน (คน)</span>',
                    style: {
                        fontSize: '20px',
                        fontFamily: 'fc_lamoonregular'
                    }
                }
            },
            credits: {
                enabled: false
            },
            series: [{
                showInLegend: false,
                colorByPoint: true,
                data: [
                    {
                        y: this.state.people_0years,
                        color: '#f45a3b'
                    },
                    {
                        y: this.state.people_1years,
                        color: '#fe9200'
                    },
                    {
                        y: this.state.people_2years,
                        color: '#fcdc00'
                    },
                    {
                        y: this.state.people_3years,
                        color: '#dbdf00'
                    },
                    {
                        y: this.state.people_4years,
                        color: '#a4dd00'
                    },
                ]

            }]
        };

        var linkStyle;
        if (this.state.hover) {
            linkStyle = { color: '#ed1212', cursor: 'pointer' }
        } else {
            linkStyle = { color: '#000' }
        }
        return (

            <div className='App'>
                <div className="Row">

                    <div className="col-12">

                        <h2 style={{ marginBottom: "0", marginTop: "10px", marginLeft: "50px" }}>
                            เลือก SE ย่อย
                        <select className="select" onChange={this.get_Cert} type="select">
                                {this.state.name_se.map((ele_get_se, index) => {
                                    return (
                                        <option value={ele_get_se.user_id}>
                                            {ele_get_se.name}
                                        </option>
                                    )
                                })}
                            </select>
                        </h2>
                        {/* <div style={{ width: 100 ,height: 100}}><MDefaultChart /></div> */}
                        <h3 style={{ textAlign: "center" }}>มาตรฐานเกษตกรในเครือ {this.state.name_neo}</h3>
                        {/* <h4>{this.state.volume_farmer.map((element)=>{
                            return(<p>จากจำนวนเกษตรกรทั้งหมด {element.sum_farmer} คน</p>)
                        })}</h4> */}
                    </div>
                </div>

                <div className="Row">

                    <div className="col-5">
                        <HighchartsReact highcharts={Highcharts} options={options2} />

                        {/* <h4 style={{ cursor: 'pointer', margin: "0", textAlign: "left", paddingLeft: "50px", marginBottom: 20 }} 
                        // onClick={() => this.filter_area_storage()}
                        >พื้นที่เพาะปลูกทีได้รับการรับรองมาตรฐาน จำนวน {this.state.people_pass} คน</h4> */}

                        <HighchartsReact highcharts={Highcharts} options={options} />
                        <h4 style={{ textAlign: "center" }}>กราฟแสดงจำนวนเกษตรกรที่ผ่านการรับรองทั้งหมด</h4>
                        <HighchartsReact highcharts={Highcharts} options={options3} />



                    </div>
                    <div className="col-1"></div>
                    <div className="col-6">
                        {/* <h3 style={{ textAlign: "center", margin: "0" }}>เลือกการเเสดงรายชื่อเกษตรกร</h3> */}


                        {/* <h6 style={{ cursor: 'pointer',margin:"0" }} onClick={() => this.setState({ get_farmer: this.state.get_origin })}>เกษตรทั้งหมด จำนวน 1347 คน</h6>
                        <h6 style={{ cursor: 'pointer',margin:"0" }} onClick={() => this.filter_chemical_date(0)}>ไม่ได้ใช้สารเคมีน้อยกว่า 1 ปี จำนวน 140 คน</h6>
                        <h6 style={{ cursor: 'pointer',margin:"0" }} onClick={() => this.filter_chemical_date(1)}>ไม่ได้ใช้สารเคมี 1 ปี จำนวน 338 คน</h6>
                        <h6 style={{ cursor: 'pointer',margin:"0" }} onClick={() => this.filter_chemical_date(2)}>ไม่ได้ใช้สารเคมี 2 ปี จำนวน 299 คน</h6>
                        <h6 style={{ cursor: 'pointer',margin:"0" }} onClick={() => this.filter_chemical_date(3)}>ไม่ได้ใช้สารเคมี 3 ปี จำนวน 400 คน</h6>
                        <h6 style={{ cursor: 'pointer',margin:"0" }} onClick={() => this.filter_chemical_date(4)}>ไม่ได้ใช้สารเคมีมากกว่า 3 ปี จำนวน 287 คน</h6> */}


                        {/* <h4 style={{ cursor: 'pointer' }} onClick={() => this.filter_area_storage()}>พื้นที่เพาะปลูกทีได้รับการรับรองมาตรฐาน จำนวน {this.state.sum_area_storage.length} คน</h4> */}
                        {/* <h4 style={{ textAlign: "center" }}>รายชื่อเกษตร </h4> */}



                        {/* ตารางแสดงข้อมูลผู้ได้รับรองมาตรฐาน */}
                        <h4>รายชื่อผู้ที่ผ่านการรับรองมาตรฐาน</h4>


                        {currentTodos_pass.length != 0 ?
                            <div>
                                <table style={{ float: "right", marginLeft: 500, textAlign: "center", marginBottom: 70, marginTop: 10 }}>
                                    <tr>
                                        <th>ลำดับ</th>
                                        <th>ชื่อ-สกุล</th>
                                        <th>การรับรองมาตรฐาน/ไร่</th>
                                        <th>พืชที่เคยปลูกได้ดี</th>
                                    </tr>
                                    {currentTodos_pass.map((element, index) => {
                                        return (
                                            <tr style={{ textAlign: "center" }} >
                                                <td>{element.num}</td>
                                                <td>{element.title} {element.first_name} {element.last_name}</td>
                                                <td >
                                                    <button type="button" onClick={() => this.certified_check(element.farmer_id)} style={{ fontFamily: "fc_lamoonregular", fontSize: "16px" }} >ดูข้อมูลเพิ่มเติม</button>
                                                </td>
                                                <td>
                                                    <button type="button" onClick={() => this.plant_type_best_show(element.farmer_id)} style={{ fontFamily: "fc_lamoonregular", fontSize: "16px" }} >ดูข้อมูลเพิ่มเติม</button>
                                                </td>
                                            </tr>
                                        )
                                    })}
                                </table>
                                <div style={{ textAlign: 'center' }}>
                                    <Pagination
                                        currentPage={currentPage}
                                        totalPages={Math.ceil(get_farmer_pass.length / todosPerPage)}
                                        changeCurrentPage={(value) => this.changeCurrentPage(value, "currentPage")}
                                        theme="square-fill"
                                    />
                                </div>
                            </div>

                            : <div>
                                <table style={{ float: "right", marginLeft: 500, textAlign: "center", marginBottom: 70, marginTop: 10 }}>
                                    <tr>
                                        <th>ลำดับ</th>
                                        <th>ชื่อ-สกุล</th>
                                        <th>การรับรองมาตรฐาน/ไร่</th>
                                        <th>พืชที่เคยปลูกได้ดี</th>
                                    </tr>
                                    <tr>
                                        <td colSpan="4">ไม่มีรายชื่อ</td>
                                    </tr>
                                </table>
                            </div>
                        }





                        {/* ตารางแสดงข้อมูลผู้ไม่ได้รับรองมาตรฐาน */}
                        <h4>รายชื่อผู้ที่ไม่ผ่านการรับรองมาตรฐาน</h4>
                        <table style={{ float: "right", marginLeft: 500, textAlign: "center", marginBottom: 70, marginTop: 10 }}>
                            <tr>
                                <th>ลำดับ</th>
                                <th>ชื่อ-สกุล</th>
                                <th>การรับรองมาตรฐาน/ไร่</th>
                                <th>พืชที่เคยปลูกได้ดี</th>
                            </tr>
                            {currentTodos_unpass.map((element, index) => {
                                return (
                                    <tr style={{ textAlign: "center" }} >
                                        <td>{element.num}</td>
                                        <td>{element.title} {element.first_name} {element.last_name}</td>
                                        <td > ไม่ผ่านการรับรอง ({element.certified.certified_array.length ? element.certified.certified_array.map((element_cer) => {
                                            return (<>{element_cer.detail}</>)
                                            
                                        }) : 0 })
                                                    {/* <button type="button" onClick={() => this.certified_check(element.farmer_id)} style={{ fontFamily: "fc_lamoonregular", fontSize: "16px" }} >ดูข้อมูลเพิ่มเติม</button> */}
                                        </td>
                                        <td>
                                            <button type="button" onClick={() => this.plant_type_best_show(element.farmer_id)} style={{ fontFamily: "fc_lamoonregular", fontSize: "16px" }} >ดูข้อมูลเพิ่มเติม</button>
                                        </td>
                                    </tr>
                                )
                            })}
                        </table>

                        <div style={{ textAlign: 'center' }}>
                            <Pagination
                                currentPage={currentPage_unpass}
                                totalPages={Math.ceil(get_farmer_unpass.length / todosPerPage_unpass)}
                                changeCurrentPage={(value) => this.changeCurrentPage(value, "currentPage_unpass")}
                                theme="square-fill"
                            />
                        </div>



                        {/* 
                        <h5 style={{ textAlign: "center", margin: "0" }}>จำนวนปีที่ห่างจากการใช้สารเคมี :
                        <select name="type_user" onChange={this.filter_chemical_date}>
                                <option value="all">ทั้งหมด</option>
                                <option value="0" >น้อยกว่า 1 ปี</option>
                                <option value="1" >1 ปี</option>
                                <option value="2" >2 ปี</option>
                                <option value="3" >3 ปี</option>
                                <option value="4" >มากกว่า 3 ปี</option>
                            </select>
                        </h5> */}

                        {/* <table>
                            <tr>
                                <th>ลำดับ</th>
                                <th>ชื่อ-สกุล</th>
                                <th>พื้นที่เพาะปลูกทีได้รับการรับรองมาตรฐาน
                                </th>
                                <th style={{ textAlign: "center" }} >ชนิดพืชท้องถิ่นที่ปลูกที่เคยปลูกได้ดี</th>
                            </tr>

                            {currentTodos_unpass.map((element, index) => {
                                let diff = moment().utc(7).add('years', 543).diff(moment(element.chemical_date), 'year', true)
                                // console.log("certified : ", element.certified)
                                // console.log("Plant Type Best : ", element.plant_type_best)
                                return (
                                    <tr style={{ textAlign: "center" }} >
                                        <td>{element.num}</td>
                                        <td>{element.title} {element.first_name} {element.last_name}</td>


                                        <td >
                                            <button type="button" onClick={() => this.certified_check(element.farmer_id)} style={{ fontFamily: "fc_lamoonregular", fontSize: "16px" }} >ดูข้อมูลเพิ่มเติม</button>
                                        </td>
                                        <td>
                                            <button type="button" onClick={() => this.plant_type_best_show(element.farmer_id)} style={{ fontFamily: "fc_lamoonregular", fontSize: "16px" }} >ดูข้อมูลเพิ่มเติม</button>
                                        </td>
                                    </tr>
                                )
                            })}
                        </table>

                        <div style={{ textAlign: 'center' }}>
                            <Pagination
                                currentPage={currentPage}
                                totalPages={Math.ceil(get_farmer.length / todosPerPage)}
                                changeCurrentPage={this.changeCurrentPage}
                                theme="square-fill"
                            />
                        </div> */}

                        <Modal open={this.state.open} onClose={this.onCloseModal} center >
                            <div style={{ fontFamily: "fc_lamoonregular" }} >
                                <h3 > พื้นที่เพาะปลูกทีได้รับการรับรองมาตรฐาน</h3>
                                <div style={{ fontFamily: "fc_lamoonregular" }}>
                                    <table >
                                        <thead>
                                            <tr>
                                                <th>ลำดับ</th>
                                                <th>รายการที่ผ่านการรับรองมาตรฐาน</th>
                                                <th>จำนวนไร่</th>
                                            </tr>
                                            {this.state.detail_certified.map((ele_cer, index) => {
                                                return (
                                                    <tr>
                                                        <td>
                                                            {index + 1}
                                                        </td>
                                                        <td>
                                                            {ele_cer.certified === "อื่นๆ" ? <>{this.state.detail_certified2.certified_array_other}</>:<>{ele_cer.certified}</>}
                                                            {console.log("อื่นๆ : ",this.state.detail_certified2.certified_array_other)}
                                                        </td>
                                                        <td>
                                                            {ele_cer.detail}   
                                                        </td>
                                                    </tr>
                                                )
                                            })}
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </Modal>

                        <Modal open={this.state.open1} onClose={this.onCloseModal} center >
                            <div style={{ fontFamily: "fc_lamoonregular" }} >
                                <h3 > ชนิดพืชท้องถิ่นที่ปลูกที่เคยปลูกได้ดี</h3>
                                <div style={{ fontFamily: "fc_lamoonregular" }}>
                                    <table >
                                        <thead>
                                            <tr>
                                                <th>ลำดับ1</th>
                                                <th>ลำดับ2</th>
                                                <th>ลำดับ3</th>
                                                <th>ลำดับ4</th>
                                                <th>ลำดับ5</th>
                                            </tr>
                                            <tr>
                                                <td>{this.state.detail_plant_best_first}</td>
                                                <td>{this.state.detail_plant_best_second}</td>
                                                <td>{this.state.detail_plant_best_third}</td>
                                                <td>{this.state.detail_plant_best_fourth}</td>
                                                <td>{this.state.detail_plant_best_fifth}</td>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </Modal>



                        <div style={{ paddingRight: "0px" }}>

                            <h4 style={{ textAlign: "center" }}>จำนวนเกษตรกรในเครือข่าย</h4>
                            <table>
                                <tr>
                                    <th>ชื่อ SE ย่อย</th>
                                    <th>จำนวนเกษตรกรในเครือ</th>
                                </tr>
                                {this.state.volume_fermer.map((element, index) => {
                                    return (
                                        <tr>
                                            <td style={{ textAlign: "center" }}>{element.se_name}</td>
                                            <td style={{ textAlign: "center" }}>{element.count_farmer}</td>
                                        </tr>
                                    )
                                })}
                                <tr>
                                    <th>รวม</th>
                                    <th>{this.sum_volume(this.state.volume_fermer)}</th>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

            </div >

        )
    }
}
export default M_Default;