import React, { Component } from 'react';
import Chart from 'react-apexcharts';



class Donut extends Component {

  constructor(props) {
    super(props);

    this.state = {
      
      series: [1, 4, 4, 0, 0],
      options: {
        chart: {
          type: 'donut',
        },
        
            labels: ["Neo firm วิสาหกิจชุมชนปลาบู่ข้าวอินทรีย์ (11.1%)", "Neo firm เครือข่ายเกษตรกรอินทรีย์อิสาน(44.4%)", "Neo firm บริษัทเก้าศิริจำกัด(44.4%)", "Neo firm บริษัทกรีนโกรทออร์แกนิค(0%)	", "Neo firm ภาคีเครือข่ายสุดยอดข้าวไทย(0%)"],
            colors: ['#eb144c', '#fccb00', '#00bcd4', '#000000', '#000000'],

      },
    
    
    };
  }

  render() {

    return (
      <div className="donut" >
        <div className="p">
        <Chart options={this.state.options} series={this.state.series} type="donut" width="700" />
      </div>
      </div>
    );
  }
}



export default Donut;