import swal from 'sweetalert';
import React, { Component } from 'react';
import Board from 'react-trello'
import Cards from './Cards'
import NewCardForm from './NewCardFrom';
import { Redirect } from 'react-router-dom'
import { user_token } from '../Support/Constance';
import { get, post } from '../Support/Service';

const data = {
    lanes: [
        {
            id: 'lane1',
            title: 'State Plant',
            // label: '2/2',
            cards: []
        }
    ]
}

class Professor extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: data,
            show: false,
            plant_id: null,
            plant_name: "",
            get_plant_phase: [],
            get_plant_phase_edit: "",
            ch_phase_st: false
        }
    }


    componentWillMount() {
        this.get_plant_phase()
    }

    componentDidMount() {
        if (this.props.location.params) {
            const { phase, plant_id, plant_name } = this.props.location.params.plant_data

            let phase_data = this.state.data

            phase_data.lanes[0].cards = phase
            this.setState({
                plant_name: plant_name,
                data: phase_data,
                plant_id: plant_id
            })
        }
    }
    get_plant_phase = async () => {
        try {
            await get('researcher/get_plant_phase', user_token).then((result) => {
                if (result.success) {
                    console.log("get_plant_phase", result.result)
                    this.setState({
                        get_plant_phase: result.result
                    })
                    setTimeout(() => {
                        console.log("get_plant_phase", result.result)
                    }, 500)
                } else {
                    console.log("get_plant_phase", result.result)
                }
            });
        } catch (error) {
            alert("get_plant_phase" + error);
        }
    }

    onDeleteCard = (data) => {
        swal({
            buttons: [true, "แน่ใจที่จะลบสเตจนี้?"],
        }); swal({
            title: "แน่ใจที่จะลบสเตจนี้?",
            text: "คุณต้องการลบสเตจนี้!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            // this.setState({show:true})

            .then((willDelete) => {
                console.log("alret : ", willDelete)

                if (willDelete) {
                    swal("ลบเรียบร้อย!", {
                        icon: "success"
                    });
                    const { laneId, id, index } = data;
                    let EditDataArray = this.state.data;
                    let lanes_index = EditDataArray.lanes.findIndex((element) => element.id == laneId)
                    EditDataArray.lanes[0].cards.splice(index, 1)

                    this.update_pahse_to_db(EditDataArray.lanes[0].cards)

                    console.log("lanes_index", EditDataArray)
                    this.setState({
                        data: EditDataArray
                    })
                } else {
                    swal("ยกเลิก!");
                }
                
            });
    }

    onEditSaveToState = (data) => {

        const { phase_name, phase_day, laneId, id, index } = data;
        let EditDataArray = this.state.data;
        let lanes_index = EditDataArray.lanes.findIndex((element) => element.id == laneId)
        console.log("lanes_index", lanes_index)
        EditDataArray.lanes[0].cards[index] = { id: id, phase_name: phase_name, phase_day: phase_day };
        this.update_pahse_to_db(EditDataArray.lanes[0].cards)

        this.setState({
            data: EditDataArray
        })

    }

    render_card = (props) => {
        return (
            <Cards {...props}
                onEditSaveToState={(data) => this.onEditSaveToState(data)}
                onDeleteCard={(data) => this.onDeleteCard(data)} />
        )

    }

    render_newCardForm = (props) => {
        console.log("new card: ", props)
        return (
            <NewCardForm {...props} />
        )
    }

    onAdd = (data) => {
        let crad_array = this.state.data.lanes[0].cards;
        crad_array.push(data)
        // console.log("card_arr", crad_array)
        if (this.update_pahse_to_db(crad_array)) {
            this.setState({
                data: this.state.data
            })
        } else {

        }

    }

    onChangePahse = () => {
        if (!this.state.ch_phase_st) {

            this.setState({
                get_plant_phase_edit: JSON.stringify(this.state.data.lanes[0].cards)
            })
            //             console.log("st",this.state.data.lanes[0].cards)
            //             this.setState((prevState)=>{
            // console.log("prev st",prevState.data.lanes[0].cards)
            //             })
        }

        this.setState({ ch_phase_st: true })
        // swal("บันทึกเรียบร้อย!", "You clicked the button!", "success");
        // window.location.href = "/Plant_page";
    }

    onChangeSave = () => {

        this.update_pahse_to_db(this.state.data.lanes[0].cards).then((value) => {


            if (value) {
                this.setState({ ch_phase_st: false })
                swal("บันทึกเรียบร้อย!", "You clicked the button!", "success");
            } else {
                swal("error!", "You clicked the button!", "warning");
            }
        }).catch((err) => {



        })
    }

    onChangeCancel = () => {
        let crad_array = this.state.data;
        crad_array.lanes[0].cards = JSON.parse(this.state.get_plant_phase_edit)
        console.log("crad_array", crad_array)
        this.setState({
            data: crad_array,
            ch_phase_st: false
        })
    }

    update_pahse_to_db = async (phase) => {
        let body = {
            plant_id: this.state.plant_id,
            phase: JSON.stringify(phase),
        }

        let return_st = false;
        try {
            await post(body, 'researcher/set_plant_phase', user_token).then((result) => {
                if (result.success) {
                    return_st = true
                } else {
                    return_st = false
                }
            });
        } catch (error) {
            return_st = false
            // alert("update_pahse_to_db" + error);
        }

        return return_st
    }

    render() {


        if (!this.props.location.params) {
            return <Redirect to="/Plant_page" />
        }


        return (
            <div className="body">
                <div className="row">
                    <div className="col-1"></div>
                    <div className="col-5">
                        <div>{this.state.plant_name}</div>
                        <NewCardForm onAdd={(data) => this.onAdd(data)} />
                        {/* {JSON.stringify(this.state.data.lanes[0].cards)} */}
                        {console.log("params : ", this.props.location.params.plant_data)}
                    </div>
                    <div className="col-5">

                        <Board
                            style={{
                                backgroundColor: 'transparent',
                                height: "100%"
                            }}
                            laneStyle={{
                                height: "75%",
                                // width:"100%",
                                backgroundColor: '#eee',
                                position: "fixed"
                            }}
                            data={this.state.data}
                            onDataChange={(event) => { this.setState({ data: event }) }}
                            // editable
                            handleDragEnd={() => { this.onChangePahse() }}
                            cardStyle={{ backgroundColor: '#24252A' }}
                            components={{ Card: this.render_card }}
                        />
                        {!this.state.ch_phase_st ?
                            <div className="row">
                                <div className="col-6">
                                    <button className="btn-save" disabled
                                        style={{ marginTop: "85%", marginLeft: "2.5%" }}
                                        onClick={() => { this.onChangeSave() }}>บันทึก</button>
                                </div>
                                <div className="col-6">
                                    <button className="btn-cancel" disabled
                                        style={{ marginTop: "85%" }}
                                        onClick={() => { this.onChangeCancel() }}>ยกเลิก</button>
                                </div>
                            </div>
                            : <div className="row">
                                <div className="col-6">
                                    <button className="btn-save"
                                        style={{ marginTop: "85%", marginLeft: "2.5%" }}
                                        onClick={() => { this.onChangeSave() }}>บันทึก</button>
                                </div>
                                <div className="col-6">
                                    <button className="btn-cancel"
                                        style={{ marginTop: "85%" }}
                                        onClick={() => { this.onChangeCancel() }}>ยกเลิก</button>
                                </div>
                            </div>}
                    </div>
                    <div className="col-1"></div>
                </div>
            </div>
        )
    }
}

export default Professor;