import React, { Component } from 'react'
import swal from 'sweetalert';
import plus from '../Image/plus.png'
import { v1 as uuidv1 } from 'uuid';

class NewCardForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            phase_name: "",
            phase_day: 0
        }
    }

    addCard = () => {
        let data = {
            id: uuidv1(),
            laneId: "lane1",
            phase_name: this.state.phase_name,
            phase_day: this.state.phase_day
        }
        console.log("asd", this.state.phase_name)
        if (this.state.phase_name != "" && this.state.phase_day != 0) {
            this.props.onAdd(data);
            swal({
                title: "เพิ่มสเตจใหม่เรียบร้อย!",
                text: "กด OK เพื่อดำเนินต่อ",
                icon: "success",
                button: "OK",
            });
        }
        else {
            swal("กรุณากรอกข้อมูลให้ครบถ้วน");
        }

    }

    render() {
        return (

            <div className="crad-edit">
                {console.log("pp :", this.props)}
                <div className="row">
                    <div className="col-1">
                        
                        <div className="add-card">
                            <img src={plus} className="img-add-card" />
                        </div>
                    </div>
                    <div className="col-5">
                        <div className="head-card">
                            <input className="input-name-card" type="text"
                                onChange={(event) => this.setState({ phase_name: event.target.value })} />
                        </div>
                    </div>
                    <div className="col-4">
                        <input className="input-day-card" type="number" min="1" style={{ marginTop: -4 }}
                            onChange={(event) => this.setState({ phase_day: event.target.value })} />

                    </div>
                    <div className="col-1" style={{ textAlign: "right", marginTop: "5px", color: "#FBFBFB" }}> วัน</div>

                </div>
                <div className="row">
                    <div className="col-1"></div>
                    <div className="col-2">
                        <button className="btn-save"
                            onClick={() => this.addCard()}>Add</button>
                    </div>
                    <div>{this.props.plant_name}</div>
                </div>
            </div>



            // <div className="card">
            //     <div className="row" style={{ height: "100%" }}>
            //         <div className="row">
            //             <div className="col-1">
            //                 <div className="add-card">+</div>
            //             </div>
            //             <div className="col-9">
            //                 <div className="head-card">
            //                     <input className="input-name-card" type="text"
            //                         onChange={(event) => this.setState({ phase_name: event.target.value })} />
            //                 </div>
            //                 <div className="row">
            //                     <div className="col-2">
            //                         <input className="input-day-card" type="number" min="1"
            //                             onChange={(event) => this.setState({ phase_day: event.target.value })} />
            //                     </div>
            //                     <div className="col-1">
            //                         วัน
            //         </div>
            //                 </div>
            //             </div>
            //             <div className="col-2">
            //                 <button className="btn-save"
            //                     onClick={() => this.addCard()}>Add</button><br />
            //                 <button className="btn-cancel"
            //                     onClick={() => this.props.onCancel()}>Cancel</button>
            //             </div>
            //         </div>
            //     </div>
            // </div>

        )
    }
}
export default NewCardForm