import React, { Component } from 'react'
import '../Style/Plant.css'
import edit from '../Image/edit1.png'
import { NavLink } from 'react-router-dom'

import { user_token } from '../Support/Constance';
import { get, post } from '../Support/Service';
const plant_data = [
    {
        plant_id: 'plant_1',
        plant_name: 'กล้วยน้ำหว่า',
        plant_qrt: 150
    }, {
        plant_id: 'plant_2',
        plant_name: 'กล้วยหอม',
        plant_qrt: 200
    }, {
        plant_id: 'plant_3',
        plant_name: 'กล้วยไข่',
        plant_qrt: 80
    }, {
        plant_id: 'plant_4',
        plant_name: 'กล้วยเล็บมือนาง',
        plant_qrt: 65
    },
    {
        plant_id: 'plant_1',
        plant_name: 'กล้วยน้ำหว่า',
        plant_qrt: 150
    }, {
        plant_id: 'plant_2',
        plant_name: 'กล้วยหอม',
        plant_qrt: 200
    }, {
        plant_id: 'plant_3',
        plant_name: 'กล้วยไข่',
        plant_qrt: 80
    }, {
        plant_id: 'plant_4',
        plant_name: 'กล้วยเล็บมือนาง',
        plant_qrt: 65
    },
    {
        plant_id: 'plant_1',
        plant_name: 'กล้วยน้ำหว่า',
        plant_qrt: 150
    }, {
        plant_id: 'plant_2',
        plant_name: 'กล้วยหอม',
        plant_qrt: 200
    }, {
        plant_id: 'plant_3',
        plant_name: 'กล้วยไข่',
        plant_qrt: 80
    }, {
        plant_id: 'plant_4',
        plant_name: 'กล้วยเล็บมือนาง',
        plant_qrt: 65
    },
    {
        plant_id: 'plant_1',
        plant_name: 'กล้วยน้ำหว่า',
        plant_qrt: 150
    }, {
        plant_id: 'plant_2',
        plant_name: 'กล้วยหอม',
        plant_qrt: 200
    }, {
        plant_id: 'plant_3',
        plant_name: 'กล้วยไข่',
        plant_qrt: 80
    }, {
        plant_id: 'plant_4',
        plant_name: 'กล้วยเล็บมือนาง',
        plant_qrt: 65
    },
]

class Plant extends Component {

    constructor(props) {
        super(props);
        this.state = {
            get_plant_phase: [],

        }
    }

    componentWillMount() {
        this.get_plant_phase()
    }

    get_plant_phase = async () => {
        try {
            await get('researcher/get_plant_phase', user_token).then((result) => {
                if (result.success) {
                    console.log("get_plant_phase", result.result)
                    this.setState({
                        get_plant_phase: result.result
                    })
                    setTimeout(() => {
                        console.log("get_plant_phase", result.result)
                    }, 500)
                } else {
                    console.log("get_plant_phase", result.result)
                }
            });
        } catch (error) {
            alert("get_plant_phase" + error);
        }
    }


    render() {
        return (
            <div className="body">
                <div className="row">
                    <div className="col-1"></div>
                    <div className="col-10">
                        <div className="dashboard-plant">
                            <div className="row">
                                <div className="col-1 dashboard-head-plant">No.</div>
                                <div className="col-6 dashboard-head-plant">Name</div>
                                <div className="col-2 dashboard-head-plant">State phase</div>
                                <div className="col-3"></div>
                            </div>


                            {this.state.get_plant_phase.map((plant_ele, plant_index) => {
                                return (
                                    <div className="deshboard-body-plant">
                                        <div className="row">
                                            <div className="col-1">{plant_index + 1}</div>
                                            <div className="col-6">{plant_ele.plant_name}</div>
                                            <div className="col-2">{plant_ele.phase.length == 0 ? "ยังไม่มีเฟสของพืชนี้"
                                                :
                                                "มีเฟสเเล้ว"
                                            }</div>
                                            <div className="col-3">
                                                {/* {"/Product/product?product_id=" + element.product_id} */}
                                                <NavLink to={{ pathname: "/Professor_page", params:{ plant_data: plant_ele } }} >

                                                    <button className="btn-plant"><img src={edit} style={{ width: "15px" }} /> Edit</button>
                                                </NavLink>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                    <div className="col-1"></div>
                </div>
            </div>
        )
    }
}
export default Plant;