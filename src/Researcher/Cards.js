import React, { Component } from 'react'
import edit from '../Image/edit.png'
import close from '../Image/close.png'
import "../Style/Cards.css"
class Cards extends Component {

    constructor(props) {
        super(props);
        this.state = {
            edit: false,
            phase_name: "",
            phase_day: "",
            show: false
        }
    }

    componentWillMount = () => {
        this.setState({
            phase_name: this.props.phase_name,
            phase_day: this.props.phase_day,
        })
    }

    onSave = () => {

        let data = {
            phase_name: this.state.phase_name,
            phase_day: this.state.phase_day,
            laneId: this.props.laneId,
            id: this.props.id,
            index: this.props.index
        }
        console.log("data : ", data)
        console.log("props", this.props)
        this.setState({
            edit: false
        })
        this.props.onEditSaveToState(data)

    }

    onCancle = () => {
        this.setState({
            edit: false,
            phase_name: this.props.phase_name,
            phase_day: this.props.phase_day
        })
    }

    delete_card = () => {

        let data = {
            laneId: this.props.laneId,
            id: this.props.id,
            index: this.props.index
        }
        this.props.onDeleteCard(data)
    }

    render() {
        return (
            <div className="row">

                {!this.state.edit ?
                    <div className="card">
                        <div className="row">
                            <div className="col-1">
                                <div className="number-card">{this.props.index + 1}</div>
                            </div>
                            <div className="col-6">
                                <div className="head-card">{this.state.phase_name}</div>
                            </div>
                            <div className="col-2" style={{ textAlign: "right", marginTop: "5px", color: "#FBFBFB" }}>
                                {this.state.phase_day}
                            </div>
                            <div className="col-1" style={{ textAlign: "right", marginTop: "5px", color: "#FBFBFB" }}> วัน</div>
                            <div className="col-1"></div>
                            <div className="col-1">
                                <button className="btn-card" onClick={() => { this.setState({ edit: !this.state.edit }) }}>
                                    <img src={edit} className="img-card" />
                                </button>
                                <button className="btn-card" onClick={() => { this.delete_card() }}>
                                    <img src={close} className="img-card" />
                                </button>
                            </div>
                        </div>
                    </div>

                    :
                    <div className="crad-edit">
                        <div className="row">
                            <div className="col-1">
                                <div className="number-card">{this.props.index + 1}</div>
                            </div>
                            <div className="col-6">
                                <div className="head-card">
                                    <input className="input-name-card" type="text"
                                        value={this.state.phase_name}
                                        onChange={(event) => this.setState({ phase_name: event.target.value })} />
                                </div>
                            </div>
                            <div className="col-2">
                                <input className="input-day-card" type="number" min="1"
                                    style={{ marginLeft: "0px", width: "100%",marginTop:"-4px" }}
                                    value={this.state.phase_day}
                                    onChange={(event) => this.setState({ phase_day: event.target.value })} />

                            </div>
                            <div className="col-1" style={{ textAlign: "right", marginTop: "5px", color: "#FBFBFB" }}> วัน</div>
                            <div className="col-2"></div>
                        </div>
                        <div className="row">
                            <div className="col-1"></div>
                            <div className="col-2">
                                <button className="btn-save"
                                    onClick={() => { this.onSave() }}>Save</button>
                            </div>
                            <div className="col-2">
                                <button className="btn-cancel"
                                    onClick={() => { this.onCancle() }}>Cancel</button>
                            </div>
                        </div>
                    </div>
                }
            </div>
        )
    }
}

export default Cards;