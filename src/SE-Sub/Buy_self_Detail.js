import React, { Component } from 'react';
import { NavLink, Link } from 'react-router-dom'
import "../Style/Buy_self.css";
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import Black from '../Image/Black.png'
import bin from '../Image/bin.png'


class Buy_self_Detail extends Component {


    render() {

        const options3 = {
            chart: {
                renderTo: 'container',
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                height: '300px',
                type: 'pie',
            },
            title: {
                text: 'จำนวนที่ขาด',
            },
            plotOptions: {
                pie: {
                    // showInLegend: true,
                    innerSize: "60%",
                    dataLabels: {
                        enabled: false,
                        distance: -14,
                        color: "white",
                        style: {
                            fontweight: "bold",
                            fontsize: 50,
                            fontFamily: 'fc_lamoonregular'
                        }
                    },
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                    }
                }
            },
            credits: {
                enabled: false
            },
            series: [
                {
                    name: "จำนวน (kg.)",
                    colorByPoint: true,
                    data: [
                        {
                            name: "ผลผลิตที่ต้องการ",
                            y: 1,
                        },
                        {
                            name: "จำนวนที่ขาด",
                            y: 4,
                        }
                    ]
                }
            ]
        }

        return (
            <div className="App">
                <h2 style={{ textAlign: "center" }}>รายละเอียดข้อมูล</h2>
                <div className="Row" >
                    <div className="col-2" ></div>
                    <div className="col-4" >
                        <table style={{ textAlign: "center"}}>
                            <tr >
                                <th>ชื่อพืช</th>
                                <th>วันที่ต้องการ</th>
                                <th>จำนวนที่มีในคลัง (kg.)</th>
                                <th>จำนวนที่ต้องการ (kg.)</th>
                                <th>จำนวนที่ขาด (kg.) </th>
                                <th>คิดเป็น%ที่มีอยู่ในคลัง</th>

                            </tr>
                            <tr>
                                <td>ข้าวกล้องดอย</td>
                                <td>25/10/2563</td>
                                <td>2000</td>
                                <td>5200</td>
                                <td style={{ color: "#FF0000" }} >3200</td>
                                <td>61.53%</td>
                            </tr>

                        </table>

                    </div>

                    <div className="col-4" >


                        <div className="h_chart">
                            <HighchartsReact highcharts={Highcharts} options={options3} />
                        </div>
                        {/* <h4 style={{ textAlign: "center" }}>จำนวนที่ขาด</h4> */}

                    </div>

                    <div className="col-2" ></div>

                </div>

                <div className="Row">
                    <div className="col-2"></div>
                    <div className="col-8">
                        <h2 style={{ textAlign: "center" }}>รายชื่อเกษตรกร</h2>
                        < table style={{ textAlign: "center" }}>
                            <tr >

                                <th>ลำดับ</th>
                                <th>ชื่อเกษตรกร</th>
                                <th>วันที่ส่งมอบ</th>
                                <th>จำนวนที่ส่งมอบได้ (kg.)</th>
                                <th>จำนวนที่ต้องการ (kg.)</th>
                                <th>จำนวนที่ขาด (kg.) </th>
                                <th>คิดเป็น%จากจำนวนที่ขาด (kg.) </th>
                                <th>คิดเป็น%จากจำนวนรวม (kg.) </th>
                                <th></th>
                                <th></th>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>นายอทิตย์ สีเหลือง</td>
                                <td>25/12/2563</td>
                                <td>100</td>
                                <td>
                                    200
                                    </td>
                                <td style={{ color: "#FF0000" }}>100</td>
                                <td>50%</td>
                                <td>3.85%</td>
                                <td><img src={Black} className="img-edit" /></td>
                                <td><img src={bin} className="img-bin" /> </td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>นาสาวจันทร์ สีเหลือง</td>
                                <td>10/8/2563</td>
                                <td>200</td>
                                <td>
                                    800
                                    </td>
                                <td style={{ color: "#FF0000" }}>800</td>
                                <td>80%</td>
                                <td>19.23%</td>
                                <td> <img src={Black} className="img-edit" /> </td>
                                <td><img src={bin} className="img-bin" /> </td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>นางสาวอังคาร สีชมพู</td>
                                <td>20/12/2563</td>
                                <td>500</td>
                                <td>
                                    1600
                                    </td>
                                <td style={{ color: "#FF0000" }}>1500</td>
                                <td>75%</td>
                                <td>38.46%</td>
                                <td> <img src={Black} className="img-edit" /> </td>
                                <td><img src={bin} className="img-bin" /> </td>
                                
                            </tr>
                        </table>
                        <div className="btn_center"  >

                            <NavLink to={{ pathname: "/Buy_self_Detail2" }} >
                                <button className="btn_click" >เพิ่มเกษตรกร</button>
                            </NavLink>

                        </div>
                    </div>
                    <div className="col-2"></div>

                </div>

            </div>
        );
    }
}
export default Buy_self_Detail