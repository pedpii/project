import React, { Component } from 'react';
import { get, post, ip } from '../Support/Service'
import { user_token, addComma } from '../Support/Constance'
import { NavLink, Link } from 'react-router-dom'
import { Button, Segment } from 'semantic-ui-react'
import "../Style/Buy_self.css";

class Buy_self extends Component {

    constructor(props) {
        super(props)
        this.state = {
            name_plant: [],

        }
    }

    get_all_plant = async () => {
        try {
            await get('neo_firm/get_plant_data', user_token).then((result) => {
                if (result.success) {
                    let name_plant_data = []

                    // let persent = (100 / element.amount_want) * data;

                    result.result.map((result_el) => {
                        if (result_el.amount_stock <= 0) {
                            result_el.amount_stock = 0
                        }
                        let wanted_volume = result_el.amount_want - result_el.amount_stock
                        let wanted_percentage

                        if (wanted_volume <= 0) {
                            wanted_volume = 0
                            wanted_percentage = 0

                        } else {
                            wanted_percentage = (100 / result_el.amount_want) * wanted_volume
                        }


                        name_plant_data.push({
                            ...result_el,
                            wanted_volume: wanted_volume,
                            wanted_percentage: wanted_percentage
                        })
                    })

                    this.setState({
                        name_plant: name_plant_data,
                        loading: false
                    })
                }
                else {
                    alert(result.error_message)
                }
            })
        }

        catch (error) {
            alert('get_allplant: ' + error)
        }
    }

    componentWillMount() {
        this.get_all_plant()
    }

    // callculate_persent = (persent) => {
    //     let return_persent
    //     switch (persent) {
    //         case persent < 0 : return_persent = <div> </div>

    //             break;

    //         default:
    //             break;
    //     }
    // }

    render() {
        return (
            <div className="App" style={{ fontFamily: "fc_lamoonregular" }} >
                <h2 style={{ textAlign: "center" }}>รายการ การสั่งซื้อสินค้าด้วยตนเอง</h2>
                <div className="Row">
                    <div className="col-2 "> </div>
                    <div className="col-8 ">
                        <table style={{ textAlign: "center" }}>
                            <tr >
                                <th>ลำดับ</th>
                                <th>ชื่อวัตถุดิบ</th>
                                <th>จำนวนที่มีในคลัง (kg.)</th>
                                {/* <th>จำนวนที่ต้องการ (kg.)</th>
                                <th>จำนวนที่ขาด (kg.) </th>
                                <th>คิดเป็น %</th> */}
                                <th></th>
                            </tr>
                            {this.state.name_plant.map((element, index) => {

                                return (

                                    <tr style={{ textAlign: "center" }}  >
                                        <td>{index + 1}</td>
                                        <td>{element.plant_name}</td>
                                        <td>{element.amount_stock}</td>
                                        {/* <td>{element.amount_want}</td>
                                        <td>{element.wanted_volume}</td>
                                {element.wanted_percentage <= 0 ?
                                <td style={{color:"green"}}>{element.wanted_percentage.toFixed(2)} %</td> :
                                <td style={{color:"red"}}>{element.wanted_percentage.toFixed(2)} %</td>} */}
                                        
                                        <td>
                                            <NavLink to={{ pathname: "/Buy_self_plane", params: { data: element } }} >
                                                <button className="btn_click" style={{ width: 140, height: 40 }}>วางแผนการเพาะปลูก</button>
                                            </NavLink>
                                        </td>
                                    </tr>
                                )
                            })}

                        </table>
                    </div>
                    <div className="col-2 ">  </div>
                </div>


            </div>

        );
    }
}
export default Buy_self