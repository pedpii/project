import React, { Component } from 'react';
import { NavLink, Link } from 'react-router-dom'
import "../Style/Buy_self.css";
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import Black from '../Image/Black.png'
import Modal from 'react-responsive-modal'
import Checkbox from '@material-ui/core/Checkbox';


class Buy_self_Detail2 extends Component {

    constructor(props) {
        super(props)
        this.state = {
            open: false,
            search_order: [],
            get_user: null,
            check_array: [],
            order_farmer: [],
            date: '',
            selectPlant: null,
            want: [],
            show_data: [],
        }
    }




    onCloseModal = () => {
        this.setState({ open: false })
    }

    openModel = (plant) => {
        let farmer = this.state.farmer
        let selectFarmer = []

        this.state.check_array.map((element) => {

            selectFarmer.push({
                title_name: farmer[element.check].title_name,
                first_name: farmer[element.check].first_name,
                last_name: farmer[element.check].last_name,
                plant: farmer[element.check].plant,
                amount: element.amount,
            })

        })
        console.log("jhjh", selectFarmer)
        this.setState({
            open: true,
            selectFarmer: selectFarmer,
            selectPlant: plant
        })
    }
    render() {

        const options3 = {
            chart: {
                renderTo: 'container',
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                height: '300px',
                type: 'pie',
            },
            title: {
                text: 'จำนวนที่ขาด',
            },
            plotOptions: {
                pie: {
                    // showInLegend: true,
                    innerSize: "60%",
                    dataLabels: {
                        enabled: false,
                        distance: -14,
                        color: "white",
                        style: {
                            fontweight: "bold",
                            fontsize: 50,
                            fontFamily: 'fc_lamoonregular'
                        }
                    },
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                    }
                }
            },
            credits: {
                enabled: false
            },
            series: [
                {
                    name: "จำนวน (kg.)",
                    colorByPoint: true,
                    data: [
                        {
                            name: "ผลผลิตที่ต้องการ",
                            y: 1,
                        },
                        {
                            name: "จำนวนที่ขาด",
                            y: 4,
                        }
                    ]
                }
            ]
        }

        return (
            <div className="App">
                <h2 style={{ textAlign: "center" }}>รายละเอียดข้อมูล</h2>
                <div className="Row" >
                    <div className="col-2" ></div>
                    <div className="col-4" >
                        <table style={{ textAlign: "center" }}>
                            <tr >
                                <th>ชื่อพืช</th>
                                <th>วันที่ต้องการ</th>
                                <th>จำนวนที่มีในคลัง (kg.)</th>
                                <th>จำนวนที่ต้องการ (kg.)</th>
                                <th>จำนวนที่ขาด (kg.) </th>
                                <th>คิดเป็น%</th>

                            </tr>
                            <tr>
                                <td>ข้าวกล้องดอย</td>
                                <td>25/10/2563</td>
                                <td>2000</td>
                                <td>5200</td>
                                <td style={{ color: "#FF0000" }} >3200</td>
                                <td>61.53%</td>
                            </tr>

                        </table>

                    </div>

                    <div className="col-4" >


                        <div className="h_chart">
                            <HighchartsReact highcharts={Highcharts} options={options3} />
                        </div>
                        {/* <h4 style={{ textAlign: "center" }}>จำนวนที่ขาด</h4> */}

                    </div>

                    <div className="col-2" ></div>

                </div>

                <div className="Row">
                    <div className="col-2"></div>
                    <div className="col-8">
                        <h2 style={{ textAlign: "center" }}>รายชื่อเกษตรกร</h2>
                        < table style={{ textAlign: "center" }}>
                            <tr >

                                <th>ลำดับ</th>
                                <th>ชื่อเกษตรกร</th>
                                <th>วันที่ส่งมอบ</th>
                                <th>จำนวนที่ส่งมอบได้ (kg.)</th>
                                <th>จำนวนที่ต้องการ (kg.)</th>
                                <th>จำนวนที่ขาด (kg.) </th>
                                <th>คิดเป็น%จากจำนวนที่ขาด (kg.) </th>
                                <th>คิดเป็น%จากจำนวนรวม (kg.) </th>
                                <th>

                                </th>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>นายอทิตย์ สีเหลือง</td>
                                <td>25/12/2563</td>
                                <td>100</td>
                                <td>
                                    200
                                    </td>
                                <td style={{ color: "#FF0000" }}>100</td>
                                <td>50%</td>
                                <td>3.85%</td>
                                <td>
                                    <img src={Black} className="img-edit" />
                                </td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>นาสาวจันทร์ สีเหลือง</td>
                                <td>10/8/2563</td>
                                <td>200</td>
                                <td>
                                    800
                                    </td>
                                <td style={{ color: "#FF0000" }}>800</td>
                                <td>80%</td>
                                <td>19.23%</td>
                                <td> <img src={Black} className="img-edit" /> </td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>นางสาวอังคาร สีชมพู</td>
                                <td>20/12/2563</td>
                                <td>500</td>
                                <td>
                                    1600
                                    </td>
                                <td style={{ color: "#FF0000" }}>1500</td>
                                <td>75%</td>
                                <td>38.46%</td>
                                <td> <img src={Black} className="img-edit" /> </td>
                            </tr>
                        </table>
                        <div className="btn_center"  >

                            <NavLink to={{ pathname: "/Buy_self_Detail2" }} >
                                <button onClick={() => this.openModel()} className="btn_click" >เพิ่มเกษตรกร</button>
                            </NavLink>
                        </div>

                        <Modal open={this.state.open} onClose={this.onCloseModal} state={{width:800}} >
                            <div className="Row">
                                <div className="col-1"></div>
                                <div className="col-10">
                                    <h2 style={{ textAlign: "center" }}>รายชื่อเกษตรกร</h2>
                                    < table style={{ textAlign: "center" }}>
                                        <tr >
                                            <th></th>
                                            <th>ลำดับ</th>
                                            <th>ชื่อเกษตรกร</th>
                                            <th>วันที่ส่งมอบ</th>
                                            <th>จำนวนที่ส่งมอบได้ (kg.)</th>
                                            <th>จำนวนที่ต้องการ (kg.)</th>
                                            <th>จำนวนที่ขาด (kg.) </th>
                                            <th>คิดเป็น%จากจำนวนที่ขาด (kg.) </th>
                                            <th>คิดเป็น%จากจำนวนรวม (kg.) </th>

                                        </tr>
                                        <tr>
                                            <td>
                                                <Checkbox inputProps={{ 'aria-label': 'uncontrolled-checkbox' }} />
                                            </td>
                                            <td>1</td>
                                            <td>นายอทิตย์ สีเหลือง</td>
                                            <td>25/12/2563</td>
                                            <td>100</td>
                                            <td>
                                                <input style={{ width: 50, height: 10, marginTop: 2 }} type="number" />
                                            </td>
                                            <td style={{ color: "#FF0000" }}>100</td>
                                            <td>50%</td>
                                            <td>3.85%</td>
                                        </tr>
                                        <tr>
                                            <td><Checkbox inputProps={{ 'aria-label': 'uncontrolled-checkbox' }} /></td>
                                            <td>2</td>
                                            <td>นาสาวจันทร์ สีเหลือง</td>
                                            <td>10/8/2563</td>
                                            <td>200</td>
                                            <td>
                                                <input style={{ width: 50, height: 10, marginTop: 2 }} type="number" />
                                            </td>
                                            <td style={{ color: "#FF0000" }}>800</td>
                                            <td>80%</td>
                                            <td>19.23%</td>
                                        </tr>
                                        <tr>
                                            <td><Checkbox inputProps={{ 'aria-label': 'uncontrolled-checkbox' }} /></td>
                                            <td>3</td>
                                            <td>นางสาวอังคาร สีชมพู</td>
                                            <td>20/12/2563</td>
                                            <td>500</td>
                                            <td>
                                                <input style={{ width: 50, height: 10, marginTop: 2 }} type="number" />
                                            </td>
                                            <td style={{ color: "#FF0000" }}>1500</td>
                                            <td>75%</td>
                                            <td>38.46%</td>
                                        </tr>
                                        <tr>
                                            <td><Checkbox inputProps={{ 'aria-label': 'uncontrolled-checkbox' }} /></td>
                                            <td>4</td>
                                            <td>นายพุทธ สีเขียว</td>
                                            <td>20/5/2563</td>
                                            <td>300</td>
                                            <td>
                                                <input style={{ width: 50, height: 10, marginTop: 2 }} type="number" />
                                            </td>
                                            <td>0</td>
                                            <td>0%</td>
                                            <td>0%</td>
                                        </tr>
                                        <tr>
                                            <td><Checkbox inputProps={{ 'aria-label': 'uncontrolled-checkbox' }} /></td>
                                            <td>5</td>
                                            <td>นายพฤหัส สีส้ม</td>
                                            <td>2/42563</td>
                                            <td>1000</td>
                                            <td>
                                                <input style={{ width: 50, height: 10, marginTop: 2 }} type="number" />
                                            </td>
                                            <td >0</td>
                                            <td>0%</td>
                                            <td>0%</td>
                                        </tr>
                                        <tr>
                                            <td><Checkbox inputProps={{ 'aria-label': 'uncontrolled-checkbox' }} /></td>
                                            <td>6</td>
                                            <td>นางสาวศุกร์ สีฟ้า</td>
                                            <td>8/4/2563</td>
                                            <td>500</td>
                                            <td>
                                                <input style={{ width: 50, height: 10, marginTop: 2 }} type="number" />
                                            </td>
                                            <td>0</td>
                                            <td>0%</td>
                                            <td>0%</td>
                                        </tr>
                                    </table>
                                    <div className="btn_center"  >

                                        <NavLink to={{ pathname: "/Buy_self_Detail" }} >
                                            <button className="btn_click" >ยืนยันการวางแผน</button>
                                        </NavLink>

                                    </div>
                                </div>

                                <div className="col-1"></div>
                            </div>
                        </Modal>
                    </div>
                    <div className="col-2"></div>

                </div>






            </div>
        );
    }
}
export default Buy_self_Detail2