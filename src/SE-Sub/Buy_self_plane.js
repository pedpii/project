import React, { Component } from 'react';
import { get, post, ip } from '../Support/Service'
import { user_token, addComma } from '../Support/Constance'

import "../Style/Buy_self.css";

import { NavLink, Link } from 'react-router-dom'
import { Button, Segment } from 'semantic-ui-react'
import Highcharts from 'highcharts'
import HighchartsReact from 'highcharts-react-official'
import Pagination from "../Support/Pagination";
// import Checkbox from '@material-ui/core/Checkbox';
import transitions from '@material-ui/core/styles/transitions';

import moment from 'moment'

import { Redirect } from 'react-router-dom'

import Checkbox from './CheckboxBuyselfPlan'

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import Black from '../Image/Black.png'
import bin from '../Image/bin.png'
import correct from '../Image/correct.png'
import edit from '../Image/edit.png'
import close from '../Image/close.png'
import right from '../Image/right.png'
import wrong from '../Image/wrong.png'


import swal from 'sweetalert';

import Modal from 'react-responsive-modal';

import { Autocomplete, TextInput, Combobox } from 'evergreen-ui'


class Buy_self_plane extends Component {

    constructor(props) {
        super(props)
        this.state = {
            // data_farmer: [],
            currentPage: 1,
            todosPerPage: 10,
            get_farmer: [],
            volume_farmer: [],
            plants: [],
            search_order: [],
            data_farmer_phase: [],
            data_plant: [],
            check_array: [],
            data_plan: [],
            edit: false,
            edit2: false,
            check_id: null,
            data_to_database: [],
            data_del_database: null,
            data_update_database: {},
            data_plan_cancel: "",
            openmodal: false,
            phase: [],
            get_user: null,
            plant_id_props: null,
            combobox_obj: null,
            openCheckbox: false,
            plant_want: 0,
            add_value: [],
            order_plan: [],
            order_plan_to_database: null,
            data_farmer_plan: []
        }
    }

    componentWillMount = () => {


        if (this.props.location.params) {
            this.setState({
                data_plant: this.props.location.params.data,
                plant_id_props: this.props.location.params.data.plant_id

            })

            this.get_data_plan()
            this.get_data_phase_name()
            this.get_order()
            this.get_data_farmer_plan()
        }
    }

    get_order = async () => {
        let plant_id = this.props.location.params.data.plant_id
        try {
            await get(`neo_firm/get_user_order/${plant_id}`, user_token).then((result) => {
                if (result.success) {
                    this.setState({
                        order_plan: result.result,
                    })
                } else {
                }
            });
        } catch (error) {
            alert("get_user_order" + error);
        }
    }

    get_data_phase_name = async () => {
        try {
            await get(`neo_firm/get_phase_name`, user_token).then((result) => {
                if (result.success) {
                    this.setState({
                        phase: result.result
                    })
                } else {
                }
            });
        } catch (error) {
            alert("get_data_phase_name" + error);
        }
    }

    render_phase_array = (phase_array) => {
        let plant_id = this.state.plant_id_props
        let phase_data_array = []

        phase_array.map((phase_array_element) => {
            if (phase_array_element.plant_id === plant_id) {
                phase_array_element.phase.map((element) => {
                    // console.log("element_if : ", element)
                    phase_data_array.push({
                        plant_id: phase_array_element.plant_id,
                        phase_name: element.phase_name,
                        phase_id: element.phase_id
                    })
                })
            }
        })

        return phase_data_array
    }

    get_data_farmer_phase = async (event) => {
        let phase_id = event.phase_id
        try {
            await get(`neo_firm/get_data_farmer_phase/${phase_id}`, user_token).then((result) => {
                if (result.success) {
                    // console.log("เกษตรกร : ",result.result)
                    this.setState({
                        data_farmer_phase: result.result,
                        check_array: result.result
                    })
                    console.log('data_farmer_phase : ', this.state.data_farmer_phase)
                } else {
                }
            });
        } catch (error) {
            alert("get_data_farmer_phase" + error);
        }
    }

    get_data_farmer_plan = async (event) => {
        let plant_id = this.props.location.params.data.plant_id
        try {
            await get(`neo_firm/get_data_farmer_plan/${plant_id}`, user_token).then((result) => {
                if (result.success) {
                    this.setState({
                        data_farmer_plan: result.result,
                    })
                } else {
                }
            });
        } catch (error) {
            alert("get_data_farmer_plan" + error);
        }
    }

    get_data_plan = async () => {
        let id = this.props.location.params.data.plant_id
        try {
            await get(`neo_firm/get_data_plan/${id}`, user_token).then((result) => {
                if (result.success) {
                    let plane_fromwant = []
                    result.result.map((e) => {
                        let value_fromwant = e.plant_value_want - e.plant_value
                        let wanted_fromwant
                        if (value_fromwant <= 0) {
                            value_fromwant = 0
                            wanted_fromwant = 0

                        } else {
                            wanted_fromwant = (100 / e.plant_value_want) * value_fromwant
                        }
                        plane_fromwant.push({
                            ...e,
                            value_fromwant: value_fromwant,
                            wanted_fromwant: wanted_fromwant
                        })
                        // console.log("plane_fromwant : ", plane_fromwant)
                    })
                    this.setState({
                        data_plan: plane_fromwant,
                    }, () => { console.log("this.state.data_plan : ", this.state.data_plan) })
                } else {
                }
            });
        } catch (error) {
            alert("get_data_plan" + error);
        }

    }

    onChangeEdit = (event) => {
        let data = event

        console.log(event)

        let data_str = JSON.stringify(this.state.data_plan)

        this.setState({
            edit: !this.state.edit,
            check_id: event.idfamer_phase,
            data_plan_cancel: data_str,
            data_update_database: data
        })
    }

    onEditCancel = () => {
        this.setState({
            data_plan: JSON.parse(this.state.data_plan_cancel),
            edit: false
        })
    }

    onChangeEdit2 = () => {
        this.setState({
            edit2: !this.state.edit,
        })
    }

    onEditCancel2 = () => {
        this.setState({
            edit2: false
        })
    }

    onEditDate = (event, index) => {
        let data = this.state.data_plan

        data[index].havest_date = moment(event).format("YYYY-MM-DD")
        this.setState({
            data_plan: data,
            data_update_database: data[index]
        })
    }

    onEditValueWant = (event, index) => {
        let data = this.state.data_plan
        let check = parseInt(event.target.value)

        if (check > 0) {
            data[index].plant_value_want = parseInt(event.target.value)
            this.setState({
                data_plan: data,
                data_update_database: data[index]
            }
                // , () => { console.log("ข้อมูลใหม่ : ", this.state.data_update_database) }
            )
        }
        else {
            swal("กรุณากรอกตัวเลข", "หรือค่าที่มากกว่า 0!")
        }

    }

    onEditSave = () => {
        swal("บันทึกเรียบร้อย!", "ทำการบันทึกข้อมูลเรียบร้อยแล้ว!", "success");
        this.setState({
            edit: false,
        }, () => {
            this.update_database()
            this.get_data_plan()
        })

    }

    onOrderValuePlan = (event, index) => {
        let value = event.target.value
        console.log("value : ", value)
        console.log("order_plan : ", this.state.order_plan)
        console.log("index : ", index)

        let data = this.state.order_plan
        data[index].order_value = parseInt(event.target.value)
        console.log("data : ", data)
        this.setState({
            order_plan: data,
            order_plan_to_database: data[index]
        }, () => { console.log("order_plan_to_database : ", this.state.order_plan_to_database) })
    }

    onEditSave2 = () => {
        swal("บันทึกเรียบร้อย!", "ทำการบันทึกข้อมูลเรียบร้อยแล้ว!", "success");
        this.setState({
            edit2: false,
        }
            , () => { this.set_user_order() }
        )

    }

    set_user_order = async () => {
        let body = this.state.order_plan_to_database

        let return_st = false;

        try {
            await post(body, 'neo_firm/set_user_order', user_token).then((result) => {
                if (result.success) {
                    return_st = true
                } else {
                    return_st = false
                }
            });
        } catch (error) {
            return_st = false
        }
        return return_st
    }

    update_database = async () => {

        let body = this.state.data_update_database

        let return_st = false;

        try {
            await post(body, 'neo_firm/update_plan', user_token).then((result) => {
                if (result.success) {
                    return_st = true
                } else {
                    return_st = false
                }
            });
        } catch (error) {
            return_st = false
        }
        return return_st
    }

    onDelete = (event, index) => {

        let database_del = event

        swal({
            buttons: [true, "แน่ใจที่จะลบสเตจนี้?"],
        }); swal({
            title: "แน่ใจที่จะลบสเตจนี้?",
            text: "คุณต้องการลบสเตจนี้!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            // console.log("alret : ", willDelete)
            if (willDelete) {
                swal("ลบเรียบร้อย!", {
                    icon: "success"
                });

                let data = this.state.data_plan
                let data_del = data
                data_del.splice(index, 1)
                // console.log("data_del", data_del)

                this.setState({
                    data_phase_farmer: data_del,
                    data_del_database: database_del
                }, () => { this.delete_database() })
            } else {
                swal("ยกเลิก!");
            }
        });
    }

    delete_database = async () => {

        let body = this.state.data_del_database

        let return_st = false;

        try {
            await post(body, 'neo_firm/delete_plan', user_token).then((result) => {
                if (result.success) {
                    return_st = true
                } else {
                    return_st = false
                }
            });
        } catch (error) {
            return_st = false
        }
        return return_st
    }

    onCloseModal = () => {
        this.setState({ openmodal: false })
    };

    insert_farmer = () => {
        this.setState({
            openmodal: true
        })
    }

    render() {

        let cal_plantwant = 100 / this.state.data_plant.amount_want

        if (cal_plantwant == Infinity) {
            cal_plantwant = 0
        } else {
            cal_plantwant = cal_plantwant
        }

        if (!this.props.location.params) {
            return <Redirect to="/Buy_self" />
        }

        const options3 = {
            chart: {
                renderTo: 'container',
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                height: '300px',
                type: 'pie',
            },
            title: {
                text: 'จำนวนที่ขาด',
                style: {
                    fontSize: '20px',
                    fontFamily: 'fc_lamoonregular'
                }
            },
            plotOptions: {
                pie: {
                    // showInLegend: true,
                    innerSize: "60%",
                    dataLabels: {
                        enabled: false,
                        distance: -14,
                        color: "white",
                        style: {
                            fontweight: "bold",
                            fontsize: 50,
                            fontFamily: 'fc_lamoonregular'
                        }
                    },
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                    }
                }
            },
            credits: {
                enabled: false
            },
            series: [
                {
                    name: "จำนวน (kg.)",
                    style: {
                        fontSize: '20px',
                        fontFamily: 'fc_lamoonregular'
                    },
                    colorByPoint: true,

                    data: [
                        {
                            name: "ผลผลิตที่มีอยู่",
                            y: this.state.data_plant.amount_stock,
                            style: {
                                fontSize: '20px',
                                fontFamily: 'fc_lamoonregular'
                            }
                        },
                        {
                            name: "จำนวนที่ขาด",
                            y: this.state.data_plant.amount_want,
                            style: {
                                fontSize: '20px',
                                fontFamily: 'fc_lamoonregular'
                            }
                        }
                    ]
                }
            ]
        }
        return (
            <div >

                <div className="App">
                    <h2 style={{ textAlign: "center", marginBottom: "5%" }}>วางแผนการเพาะปลูก</h2>

                    <div className="Row" >
                        <div className="col-1" ></div>
                        <div className="col-6" >
                            {/* ตาราง คตก. */}
                            <div className="bg_planephase" >
                                <div style={{ backgroundColor: "#ffffff" }}  >
                                    {this.state.order_plan.map((element, index) => {
                                        return (
                                            <>
                                                <table style={{ textAlign: "center" }}>

                                                    <tr >
                                                        <th>ชื่อพืช</th>
                                                        <th>จำนวนที่มีในคลัง (kg.)</th>
                                                        <th>จำนวนที่ลูกค้าต้องการ (kg.)</th>
                                                        {/* <th>จำนวนที่ขาด (kg.) </th>
                                                        <th>คิดเป็น%ที่มีอยู่ในคลัง</th> */}
                                                        <th></th>

                                                    </tr>
                                                    <tr>
                                                        <td>{this.state.data_plant.plant_name}</td>
                                                        <td>{this.state.data_plant.amount_stock}</td>
                                                        {!this.state.edit2 ?
                                                            <>
                                                                <td>
                                                                    {element.order_value}
                                                                </td>
                                                                <button className="img-table2" onClick={() => this.setState({ edit2: !this.state.edit2 })}>
                                                                    <img src={Black} className="img-card" />
                                                                </button>

                                                            </>
                                                            :
                                                            <>
                                                                <td>
                                                                    <input type="number"
                                                                        value={element.order_value}
                                                                        onChange={(event) => { this.onOrderValuePlan(event, index) }}

                                                                    />
                                                                    <button className="img-table2" onClick={() => { this.onEditSave2() }}>
                                                                        <img src={right} className="img-card" />
                                                                    </button>
                                                                    <button className="img-table2" style={{ marginRight: 10 }} onClick={() => { this.onEditCancel2() }}>
                                                                        <img src={wrong} className="img-card" />
                                                                    </button>
                                                                </td>
                                                                <td></td>
                                                            </>
                                                        }

                                                        {/* <td style={{ color: "#FF0000" }} >{this.state.data_plant.amount_want > this.state.data_plant.amount_stock ? this.state.data_plant.amount_want - this.state.data_plant.amount_stock : 0}</td>
                                                        <td>{this.state.data_plant.wanted_percentage.toFixed(2)} % </td> */}
                                                    </tr>
                                                </table>
                                            </>
                                        )
                                    })}
                                </div>

                                {/* <div>
                                    <div className="Row" >
                                        <div className="col-2" style={{ marginBottom: 30 }} >
                                            <h4    > กรุณาเลือกเฟสพืช : </h4>
                                        </div>
                                        <div className="col-4" style={{ marginTop: 35 }}  >
                                            <div >
                                                <Combobox
                                                    openOnFocus
                                                    placeholder="เฟสพืช"
                                                    style={{ width: 400, marginLeft: 10 }}
                                                    items={this.render_phase_array(this.state.phase)}
                                                    itemToString={item => item ? item.phase_name : ""}
                                                    onChange={(event) => {
                                                        this.setState({
                                                            combobox_obj: event,
                                                            openCheckbox: true
                                                        }, () => {
                                                            // console.log("combobox_obj : ", this.state.combobox_obj)
                                                        })
                                                        this.get_data_farmer_phase(event)
                                                    }}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div> */}
                            </div>
                        </div>

                        {/* กราฟ คตก. */}
                        <div className="col-4" >
                            <div className="bg_rarph"  >
                                <div className="h_chart">
                                    <HighchartsReact highcharts={Highcharts} options={options3} />
                                </div>
                            </div>
                        </div>
                    </div>


                    <div className="Row" style={{ marginTop: "5%" }}>
                        <div className="col-2"  >   </div>
                        <div className="col-8"  >
                            <h2 style={{ textAlign: "center", marginBottom: "5%" }}>รายชื่อเกษตรกร</h2>
                            <Checkbox
                                option={this.state.data_farmer_plan}
                                plant_id={this.state.plant_id_props}
                            />

                        </div>
                        <div className="col-2"  > </div>
                    </div>

                    <div className="Row" style={{ marginTop: "5%" }}>
                        <div className="col-2"  >   </div>
                        <div className="col-8"  >
                            <h2 style={{ textAlign: "center", marginBottom: "5%" }}>รายชื่อเกษตรกร</h2>
                            <table>
                                <tr>
                                    <th>ชื่อ - นามสกุล</th>
                                    <th>วันที่</th>
                                    <th>จำนวนที่ต้องการ</th>
                                    <th></th>
                                </tr>

                                {this.state.data_plan.map((element, index) => {
                                    return (
                                        <>
                                            {!this.state.edit ?
                                                <>
                                                    <tr>
                                                        <td>{element.first_name} {element.last_name}</td>
                                                        <td>{element.havest_date}</td>
                                                        <td>{element.plant_value_want}</td>
                                                        <td>
                                                            <button className="btn-card2" style={{ marginTop: 40 }} onClick={() => { this.onChangeEdit(element) }}>
                                                                <img src={edit} className="img-card2" />
                                                            </button>
                                                            <button className="btn-card2" onClick={() => { this.onDelete(element, index) }}>
                                                                <img src={close} className="img-card2" />
                                                            </button>
                                                        </td>
                                                    </tr>
                                                </>
                                                :
                                                <>
                                                    {this.state.check_id === element.idfamer_phase ?
                                                        <>
                                                            <tr>
                                                                <td>{element.first_name} {element.last_name}</td>
                                                                <td><DatePicker
                                                                    dateFormat="dd/MM/yyyy"
                                                                    selected={moment(element.havest_date, "YYYY/MM/DD").toDate()}
                                                                    onChange={(event) => { this.onEditDate(event, index) }}
                                                                    style={{
                                                                        height: 15,
                                                                        marginTop: 20
                                                                    }}
                                                                /></td>
                                                                <td><input style={{ width: 50, height: 10, marginTop: 2 }}
                                                                    type="number"
                                                                    value={element.plant_value_want}
                                                                    onChange={(event) => { this.onEditValueWant(event, index) }}
                                                                /></td>
                                                                <td>
                                                                    <button className="img-table" onClick={() => { this.onEditSave() }}>
                                                                        <img src={right} className="img-card" />
                                                                    </button>
                                                                    <button className="img-table" style={{ marginRight: 10 }} onClick={() => { this.onEditCancel() }}>
                                                                        <img src={wrong} className="img-card" />
                                                                    </button>
                                                                </td>
                                                            </tr>
                                                        </>
                                                        :
                                                        <>
                                                            <tr>
                                                                <td>{element.first_name} {element.last_name}</td>
                                                                <td>{element.havest_date}</td>
                                                                <td>{element.plant_value_want}</td>
                                                                <td>

                                                                </td>
                                                            </tr>
                                                        </>
                                                    }
                                                </>
                                            }
                                        </>
                                    )
                                })}


                            </table>
                        </div>
                        <div className="col-2"  > </div>
                    </div>










                    {/* เลือกเฟสพืช */}
                    <div className="Row">
                        {/* ฝั่งซ้าย */}
                        <div className="col-6" style={{ marginLeft: "5%" }}>

                            {this.state.openCheckbox ?
                                <>
                                    <h2 style={{ textAlign: "center", marginTop: 100 }}>รายชื่อเกษตรกร</h2>

                                    <Checkbox
                                        option={this.state.data_farmer_phase}
                                        data={this.state.data_plan}
                                        plant_id={this.state.plant_id_props}
                                        get_data_plan={() => { this.get_data_plan() }}

                                    />
                                </>
                                :
                                <>
                                </>}
                        </div>

                        {/* ฝั่งขวา */}
                        {/* <div className="col-6" style={{ marginLeft: "5%", marginRight: "5%", marginTop: "5%" }}>
                            <div className="bg_rightplane">
                                <h2 style={{ marginTop: 20, textAlign: "center" }}>ข้อมูลการวางแผนด้วยตนเอง </h2>
                                <div className="overflow_right1" >
                                    {this.state.data_plan.map((element, index) => {
                                        return (
                                            <>
                                                {!this.state.edit ?
                                                    <>
                                                        <div className="Card_datafaemer">
                                                            <div className="Row" >
                                                                <div className="col-10">
                                                                    <div style={{ fontSize: 22, marginLeft: 20 }}>
                                                                        <div> ชื่อ :  {element.first_name} {element.last_name} </div>
                                                                        <div> ชนิดพืช : {element.phase_name} </div>
                                                                        <div> จำนวนที่มี : {element.plant_value}  กิโลกรัม </div>
                                                                        <div> จำนวนที่ต้องการ : {element.plant_value_want}  กิโลกรัม </div>
                                                                        <div> วันที่ส่งมอบ : {element.havest_date}  </div>
                                                                        <div> จำนวนที่ขาด : {element.value_fromwant}     </div>
                                                                        <div> คิดเป็น%จากจำนวนที่ขาด : {element.wanted_fromwant.toFixed(2)}%   </div>
                                                                        <div> คิดเป็น%จากจำนวนรวม : {element.plant_value_want * cal_plantwant}% </div>
                                                                    </div>
                                                                </div>
                                                                <div className="col-2" >
                                                                    <button className="btn-card2" style={{ marginTop: 40 }} onClick={() => { this.onChangeEdit(element) }}>
                                                                        <img src={edit} className="img-card2" />
                                                                    </button>
                                                                    <button className="btn-card2" onClick={() => { this.onDelete(element, index) }}>
                                                                        <img src={close} className="img-card2" />
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </>
                                                    :

                                                    <>
                                                        {this.state.check_id === element.idfamer_phase ?
                                                            <>
                                                                <div className="Card_datafaemer">
                                                                    <div className="Row" >
                                                                        <div className="col-10">
                                                                            <div style={{ fontSize: 22, marginLeft: 20 }}>
                                                                                <div> ชื่อ :  {element.first_name} {element.last_name} </div>
                                                                                <div> ชนิดพืช : {element.phase_name} </div>
                                                                                <div> จำนวนที่มี : {element.plant_value}  กิโลกรัม </div>
                                                                                <div> จำนวนที่ส่งมอบ :
                                                                                    <input style={{ width: 50, height: 10, marginTop: 2 }}
                                                                                        type="number"
                                                                                        value={element.plant_value_want}
                                                                                        onChange={(event) => { this.onEditValueWant(event, index) }}
                                                                                    /> กิโลกรัม
                                                                                </div>
                                                                                <div> วันที่ส่งมอบ : <DatePicker
                                                                                    dateFormat="dd/MM/yyyy"
                                                                                    selected={moment(element.havest_date, "YYYY/MM/DD").toDate()}
                                                                                    onChange={(event) => { this.onEditDate(event, index) }}
                                                                                    style={{
                                                                                        height: 15,
                                                                                        marginTop: 20
                                                                                    }}
                                                                                />
                                                                                </div>
                                                                                <div> จำนวนที่ขาด : {element.value_fromwant}     </div>
                                                                                <div> คิดเป็น%จากจำนวนที่ขาด : {element.wanted_fromwant}%   </div>
                                                                                <div> คิดเป็น%จากจำนวนรวม :  {element.plant_value_want * cal_plantwant}%   </div>
                                                                            </div>
                                                                        </div>
                                                                        <div className="col-2" >
                                                                            <button className="img-table" onClick={() => { this.onEditSave() }}>
                                                                                <img src={right} className="img-card" />
                                                                            </button>
                                                                            <button className="img-table" style={{ marginRight: 10 }} onClick={() => { this.onEditCancel() }}>
                                                                                <img src={wrong} className="img-card" />
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </>
                                                            :
                                                            <>
                                                                <div className="Card_datafaemer">
                                                                    <div className="Row" >
                                                                        <div className="col-10">
                                                                            <div style={{ fontSize: 22, marginLeft: 20 }}>
                                                                                <div> ชื่อ :  {element.first_name} {element.last_name} </div>
                                                                                <div> ชนิดพืช : {element.phase_name} </div>
                                                                                <div> จำนวนที่มี : {element.plant_value}  กิโลกรัม </div>
                                                                                <div> จำนวนที่ต้องการ : {element.plant_value_want}  กิโลกรัม </div>
                                                                                <div> วันที่ส่งมอบ : {element.havest_date}  </div>
                                                                                <div> จำนวนที่ขาด : {element.value_fromwant}     </div>
                                                                                <div> คิดเป็น%จากจำนวนที่ขาด : {element.wanted_fromwant}%   </div>
                                                                                <div> คิดเป็น%จากจำนวนรวม : 100% </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </>
                                                        }
                                                    </>
                                                }
                                            </>
                                        )
                                    })}



                                </div>

                            </div>
                        </div> */}

                    </div>

                </div>

            </div>

        );
    }
}
export default Buy_self_plane