import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import { get, post, ip } from '../Support/Service'
import { user_token, sortData } from '../Support/Constance'
import "../Style/S_State_show.css"
import moment from 'moment'

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

class Set_phase extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            data_plant: [],
            phase_array: [],
            startDate: moment().format("YYYY/MM/DD"),
            // markedDate: moment(new Date()).format("DD-MM-YYYY"),
            data_array: [],
            plant_value: 0,
            date_data: this.props.data_phase_farmer.havest_date
        }
    }

    render() {

        const { phase_name, phase_day, index, farmer_phase } = this.props.data_phase_farmer

        return (
            <div>

                <div className="col-5 ">

                    <div className="Card_input_phase" style={{ width: 500, height: 200, backgroundColor: "#24252A", borderRadius: 10, marginTop: 20 }}>

                        <div className="Row">
                            <div className="col-1"></div>
                            <div className="col-8">
                                <div style={{ marginTop: "5%", fontSize: 24, color: "#ffffff", lineHeight: 1.5, marginBottom: 10 }}>
                                    <div>ชื่อพืช : {phase_name}</div>
                                    <div>วันส่งมอบ : ({phase_day}) วัน</div>
                                </div>
                                <div style={{ fontSize: 24, color: "#ffffff" }}> กรอกวันที่ :
                                        <DatePicker
                                        dateFormat="dd/MM/yyyy"
                                        selected={moment(this.props.data_phase_farmer.havest_date, "YYYY/MM/DD").toDate()}
                                        onChange={(event) => { this.props.set_date_phase_component(event) }}
                                        style={{
                                            height: 15,
                                            marginTop: 20
                                        }}
                                    />
                                </div>

                                <div style={{ fontSize: 24, color: "#ffffff" }}>จำนวน :
                                    {/* {this.props.data_phase_farmer.farmer_phase? this.props.data_phase_farmer.farmer_phase.plant_value : 0} */}
                                    <input style={{ width: 200, height: 18, marginTop: 10 }} type="number"
                                        value={this.props.data_phase_farmer.plant_value}
                                        onChange={(event) => { this.props.input_value_phase(event) }}
                                    />

                                    กก.</div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default Set_phase