import React, { Component } from 'react';
import moment from 'moment'

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
// import Checkbox from '@material-ui/core/Checkbox';
// import Pagination from "../Support/Pagination";
import swal from 'sweetalert';

import { get, post, ip } from '../Support/Service'
import { user_token, sortData } from '../Support/Constance'

// import { Autocomplete, TextInput, Combobox } from 'evergreen-ui'

import "../Style/Buy_self.css";


class CheckboxBuyselfPlan extends Component {

    constructor(props) {
        super(props);
        this.state = {
            currentPage: 1,
            todosPerPage: 10,
            data_array: [],
            data_array_push: [],
            data_show: [],
            currentPage: 1,
            todosPerPage: 10,
            get_farmer: [],
            checkInput: [],
            plane_fromwant: [],
            search_farmer: [],
            check_swal: [],
            plant_value_want: []

        }
    }

    check_input = (number) => {
        let checkInput = this.state.checkInput
        // console.log('index', number)
        if (checkInput.length === 0) {
            checkInput.push({
                number
            })
        }
        else {
            let index = checkInput.findIndex((array_event) => {
                return array_event.number === number
            })
            if (index !== -1) {
                checkInput.splice(index, 1)
            }
            else {
                checkInput.push({
                    number,
                })
            }
        }
        this.setState({
            checkInput: checkInput
        })

    }

    filterFarmer = (e) => {

        var updatedList = this.props.option;
        updatedList = updatedList.filter(function (item) {
            // return item.first_name.search(e.target.value) !== -1;
            return item.first_name.toLowerCase().search(
                e.target.value.toLowerCase()) !== -1;
        });

        this.setState({
            search_farmer: updatedList,
        });
        console.log("kkkkkkkkkkkkkk : ", updatedList)

    }

    onCheck = (event) => {
        let data = event
        let data_array = this.state.data_array

        let index = data_array.findIndex((array_event) => array_event.farmer_id === data.farmer_id)
        if (index !== -1) {
            data_array.splice(index, 1)
            this.setState({
                data_array: data_array
            }, () => { console.log("ติ๊กออก", this.state.data_array) })
        }
        else {
            data_array.push(data)
            this.setState({
                data_array: data_array
            }, () => { console.log("ติ๊กเข้า", this.state.data_array) })
        }

    }

    input_value = (event, element) => {
        let check = parseInt(event.target.value)

        if (check > 0) {
            let data = element
            let value = this.state.data_array

            let index_id = value.findIndex((e) => e.farmer_id === data.farmer_id)

            value[index_id].plant_id = this.props.plant_id
            value[index_id].plant_value_want = parseInt(event.target.value)

            this.setState({
                data_array: value
            }
                , () => { console.log("ข้อมูลใส่จำนวนเลข : ", this.state.data_array) }
            )
        }
        else {
            swal("กรุณากรอกตัวเลข", "หรือค่าที่มากกว่า 0!")
        }


    }



    set_date = (event, element) => {
        // console.log("date_event",moment(event).format("YYYY-MM-DD"))

        let data = element
        let date_set = this.state.data_array

        let index_id = date_set.findIndex((e) => e.farmer_id === data.farmer_id)



        date_set[index_id].plant_id = this.props.plant_id


        date_set[index_id].havest_date = moment(event).format("YYYY-MM-DD")
        // console.log("date_set[index_id]",date_set[index_id])

        this.setState({
            data_array: date_set,
        }
            , () => { console.log("ข้อมูลใส่วันที่ : ", this.state.data_array) }
        )
    }

    onSave = async () => {
        let body = {
            neofirm_plan: this.state.data_array
        }


        let return_st = false;
        swal("เพิ่มข้อมูลเรียบร้อย!", "ทำการบันทึกข้อมูลเรียบร้อยแล้ว!", "success");

        try {
            await post(body, 'neo_firm/set_data_plan', user_token).then((result) => {
                if (result.success) {
                    this.props.get_data_plan()
                    console.log("result", result)
                    return_st = true
                } else {
                    return_st = false
                    swal("error!", "You clicked the button!", "warning");
                }
            });
        } catch (error) {
            return_st = false
        }
        return return_st
    }

    changeCurrentPage = numPage => {
        this.setState({ currentPage: numPage });
    };

    render() {
        let todos = []
        const { get_farmer, currentPage, todosPerPage, checkInput } = this.state;
        this.props.option.map((element, index) => {
            todos.push({
                num: index + 1,
                ...element
            })
        }
        )
        const indexOfLastTodo = currentPage * todosPerPage;
        const indexOfFirstTodo = indexOfLastTodo - todosPerPage;
        const currentTodos = todos.slice(indexOfFirstTodo, indexOfLastTodo);

        return (
            <div>
                {/* {console.log("data_props : ",this.props.option)} */}
                {/* <div className="col-2"></div> */}
                <input type="text" placeholder="ค้นหาเกษตรกร" onChange={this.filterFarmer} style={{ width: "98%", marginBottom: 20 }} />
                
                <div className="bg_table"  >
                    <>
                        < table style={{ textAlign: "center" }}>
                            <tr >
                                <th></th>
                                <th>ชื่อเกษตรกร</th>
                                <th>วันที่ส่งมอบ</th>
                                {/* <th>จำนวนที่ส่งมอบได้ (kg.)</th> */}
                                <th>จำนวนที่ต้องการ (kg.)</th>
                                {/* <th>จำนวนที่ขาด (kg.) </th>
                                <th>คิดเป็น%จากจำนวนที่ขาด (kg.) </th>
                                <th>คิดเป็น%จากจำนวนรวม (kg.) </th> */}

                            </tr>
                            {this.state.search_farmer.length != 0 ? this.state.search_farmer.map((element, index) => {
                                let chInput = 0
                                return (

                                    <tr>
                                        <td>
                                            <input type="checkbox"
                                                onChange={() => this.check_input(index)}
                                                onClick={() => { this.onCheck(element) }}
                                            />

                                        </td>
                                        <td>{element.first_name} {element.last_name}</td>
                                        <td>
                                            <DatePicker
                                                dateFormat="dd/MM/yyyy"
                                                selected={moment(element.havest_date, "YYYY/MM/DD").toDate()}
                                                onChange={(event) => { this.set_date(event, element) }}
                                                style={{
                                                    height: 15,
                                                    marginTop: 20
                                                }}
                                            />
                                        </td>
                                        <td>
                                            {checkInput.map((ele_check) => {
                                                if (ele_check.number == index) {
                                                    chInput = 1
                                                }
                                            }),
                                                chInput ? <input id='amount' name={element.first_name + " " + element.last_name} type="number" onChange={(event) => { this.input_value(event, element) }} /> :
                                                    <input id='amount' name={element.first_name + " " + element.last_name} type="number" value={element.plant_value_want} onChange={(event) => { this.input_value(event, element) }} disabled />
                                            }
                                            {/* <input style={{ width: 50, height: 10, marginTop: 2 }} type="number"
                                                onChange={(event) => { this.input_value(event, element) }}
                                            /> */}
                                        </td>
                                        {/* <td style={{ color: "#FF0000" }}>0</td>
                                        <td>0%</td>
                                        <td>0%</td> */}
                                    </tr>
                                )

                            })
                                : this.props.option.map((element, index) => {
                                    let chInput = 0
                                    return (
                                        <tr>
                                            <td>
                                                <input type="checkbox"
                                                    onChange={() => this.check_input(index)}
                                                    onClick={() => { this.onCheck(element) }}
                                                />

                                            </td>
                                            <td>{element.first_name} {element.last_name}</td>
                                            <td>
                                                <DatePicker
                                                    dateFormat="dd/MM/yyyy"
                                                    selected={moment(element.havest_date, "YYYY/MM/DD").toDate()}
                                                    onChange={(event) => { this.set_date(event, element) }}
                                                    style={{
                                                        height: 15,
                                                        marginTop: 20
                                                    }}
                                                />
                                            </td>
                                            {/* <td>{element.plant_value}</td> */}
                                            <td>
                                                {checkInput.map((ele_check) => {
                                                    if (ele_check.number == index) {
                                                        chInput = 1
                                                    }
                                                }),
                                                    chInput ? <input id='amount' name={element.first_name + " " + element.last_name} type="number"  onChange={(event) => { this.input_value(event, element) }} /> :
                                                        <input id='amount' name={element.first_name + " " + element.last_name} type="number" value={element.plant_value_want} onChange={(event) => { this.input_value(event, element) }} disabled />
                                                }
                                            </td>
                                        </tr>
                                    )
                                })}

                        </table>
                    </>

                </div>
                <button className="btn_click "
                    style={{ width: 150, height: 40, marginTop: 40, marginLeft: "40%", marginBottom: 20 }}
                    onClick={() => { this.onSave() }}
                >
                    ยืนยันการวางแผน
                </button>
            </div>
        );
    }
}

export default CheckboxBuyselfPlan;