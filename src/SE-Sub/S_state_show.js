import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import { get, post, ip } from '../Support/Service'
import { user_token, sortData } from '../Support/Constance'
import TextField from '@material-ui/core/TextField';
import { Autocomplete, TextInput, Combobox } from 'evergreen-ui'
import Board from 'react-trello'
import moment from 'moment'
import "../Style/S_State_show.css";
// import persor from '../Image/Person-Icon.png'
import Set_phase from './S_state_set_phase'

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import edit from '../Image/edit.png'
import close from '../Image/close.png'

import swal from 'sweetalert';

class S_state_show extends Component {
    constructor() {
        super();
        this.state = {
            data_farmer: {
                address: { province: "", district: "", district_tambons: "", detail: "", postcode: "" },
                farmer_id: null,
                first_name: "",
                last_name: "",
                phone_number: "",
                title_name: "",
                user_id: null,
            },
            data_plant: [],
            value: "",
            data_phase_farmer: [],
            data_phase_farmer_cancel: "",
            page: false,
            name_plant: "",
            phase_array: [],
            startDate: moment().format("YYYY/MM/DD"),
            combobox_obj: null,
            edit: false,
            phase_id_check: null,
            data_to_database: [],
            data_del_database: null,
            data_update_database: {},
        }
    }

    set_date = (event) => {
        // console.log("event date: ", moment(event).format("YYYY/MM/DD"))
        this.setState({
            startDate: moment(event).format("YYYY/MM/DD"),
        }, () => {
            this.set_data_phase(this.state.combobox_obj)
        });

    };

    set_date_phase_component = (event, index) => {
        let phase_array = this.state.phase_array
        // console.log("phase_array : ",phase_array)
        phase_array[index].havest_date = moment(event).format("YYYY-MM-DD")

        this.setState({
            phase_array: phase_array
        })
    }

    input_value_phase = (event, index) => {
        let value = this.state.phase_array
        value[index].plant_value = event.target.value
        this.setState({
            phase_array: value
        }
            // , () => { console.log("phase_array_value : ", this.state.phase_array) }
        )
    }

    onSave = () => {
        let data_change = this.state.phase_array
        let select_plant_obj = this.state.combobox_obj
        // console.log("select_plant_obj : ", select_plant_obj)
        let phase_array_data = []
        let phase_array_push = []
        let data_phase_farmer = this.state.data_phase_farmer
        let select_plant_obj_show = data_change.filter(phase => phase.plant_value > 0)
        // console.log("select_plant_obj_show",select_plant_obj_show)

        let index_id = data_phase_farmer.findIndex((e) => e.plant_id === select_plant_obj.plant_id)

        if (index_id >= 0) {
            select_plant_obj_show.map((select_plant_obj_element) => {
                phase_array_data.push({
                    idfamer_phase: select_plant_obj_element.idfamer_phase,
                    havest_date: select_plant_obj_element.havest_date,
                    phase_id: select_plant_obj_element.id,
                    phase_name: select_plant_obj_element.phase_name,
                    plant_value: select_plant_obj_element.plant_value,
                })
            },
                phase_array_push.push({
                    farmer_id: this.props.location.params.data.farmer_id,
                    // idfamer_phase: select_plant_obj.idfamer_phase,
                    plant_id: select_plant_obj.plant_id,
                    plant_name: select_plant_obj.label,
                    phase: phase_array_data
                })
            )
            data_phase_farmer[index_id].phase = phase_array_data //ข้อมูลที่เพิ่มมาใหม่ ตัวแปลตรง
            console.log(phase_array_push)
            // console.log("data_phase_farmer : ", data_phase_farmer)
        }
        else {
            let index = 0
            select_plant_obj_show.map((select_plant_obj_element) => {
                console.log(select_plant_obj_show)
                phase_array_data.push({
                    idfamer_phase: select_plant_obj_element.idfamer_phase,
                    havest_date: select_plant_obj_element.havest_date,
                    phase_id: select_plant_obj_element.id,
                    phase_name: select_plant_obj_element.phase_name,
                    plant_value: select_plant_obj_element.plant_value,
                })
            },
                phase_array_push.push({
                    farmer_id: this.props.location.params.data.farmer_id,
                    plant_id: select_plant_obj.plant_id,
                    plant_name: select_plant_obj.label,
                    phase: phase_array_data
                })
            )

            console.log("phase_array_push", phase_array_push)
            data_phase_farmer.push(phase_array_push[index])
            // console.log(data_phase_farmer)
        }

        this.setState({
            data_phase_farmer: data_phase_farmer,
            data_to_database: phase_array_push
        }
            // ,()=>{console.log("this.state.data_to_database",this.state.data_to_database)}
            , () => this.onSavetodata()
        )

        // console.log("ข้อมูลใหม่ : ", this.state.data_phase_farmer)


    }

    onChangecancel = () => {
        let data = this.state.data_phase_farmer
        console.log("Function cancel : ", data)
    }

    componentWillMount() {
        // this.get_data_farmer()
        this.get_data_plant()

    }

    get_data_plant = async () => {
        try {
            await get('neo_firm/get_plant_phase', user_token).then((result) => {
                if (result.success) {

                    this.setState({
                        data_plant: result.result
                    })

                    console.log('data_plant : ', this.state.data_plant)
                } else {
                }
            });
        } catch (error) {
            alert("data_plant" + error);
        }
    }

    set_data_phase = (event) => {
        let plant_index = this.state.data_phase_farmer.findIndex((e) => e.plant_id === event.plant_id)

        let data = []

        if (plant_index >= 0) {

            let date_add = 0
            event.phase.map((phase_element) => {
                date_add += parseInt(phase_element.phase_day)
                let phase_index = this.state.data_phase_farmer[plant_index].phase.findIndex((e) => e.phase_id === phase_element.id)
                let phase_obj = null
                if (plant_index >= 0 && phase_index >= 0) {
                    phase_obj = {
                        idfamer_phase: this.state.data_phase_farmer[plant_index].phase[phase_index].idfamer_phase,
                        havest_date: moment(this.state.data_phase_farmer[plant_index].phase[phase_index].havest_date, "YYYY-MM-DD").format("YYYY/MM/DD"),
                        plant_value: this.state.data_phase_farmer[plant_index].phase[phase_index].plant_value
                    }
                } else {
                    phase_obj = {
                        havest_date: moment(this.state.startDate, "YYYY/MM/DD").add(date_add, 'days').format("YYYY/MM/DD"), // date value
                        plant_value: 0
                    }
                }
                data.push({
                    ...phase_element,
                    ...phase_obj
                })

            })
        } else {
            let date_add = 0
            event.phase.map((phase_element) => {
                date_add += parseInt(phase_element.phase_day)
                data.push({
                    ...phase_element,
                    havest_date: moment(this.state.startDate, "YYYY/MM/DD").add(date_add, 'days').format("YYYY/MM/DD"), // date value
                    plant_value: 0
                })
            })
        }
        this.setState({
            page: true,
            name_plant: event.label,
            phase_array: data,
            plant_id: event.plant_id
        });
    }

    get_data_farmer = async () => {
        try {
            await get('neo_firm/get_farmer', user_token).then((result) => {
                if (result.success) {
                    // if(result.farmer_id == this.props.location.params.data){
                    //     console.log("result : ",result.result)
                    // }
                    this.setState({
                        data_farmer: result.result,
                    })
                    console.log('data_farmer : ', this.state.data_farmer)
                } else {
                }
            });
        } catch (error) {
            alert("get_data_farmer" + error);
        }
    }

    get_phase_farmer = async (farmer_id) => {
        try {
            await get(`neo_firm/get_farmer_phase/${farmer_id}`, user_token).then((result) => {
                if (result.success) {
                    this.setState({
                        data_phase_farmer: result.result,
                    })
                    // console.log('get_data_phase_farmer : ', result.result)
                } else {
                }
            });
        } catch (error) {
            alert("get_phase_farmer" + error);
        }
    }

    //เรียกข้อมูลพืชจากดาต้าเบส
    render_plant_array = (plant_array) => {
        let return_plant_array = []
        plant_array.map((element) => {
            return_plant_array.push(
                {
                    plant_id: element.plant_id,
                    label: element.plant_name,
                    phase: element.phase
                }
            )
            // console.log("return_plant_array : ",return_plant_array)
        })
        return return_plant_array
        
    }

    componentDidMount = () => {
        if (this.props.location.params) {
            this.setState({
                data_farmer: this.props.location.params.data
            })
            this.get_phase_farmer(this.props.location.params.data.farmer_id)
        }
    }

    farmer_phase_compare = (phase_id, plant_id) => {

        let plant_index = this.state.data_phase_farmer.findIndex((e) => e.plant_id === plant_id)
        let return_data = null
        if (plant_index >= 0) {
            let phase_index = this.state.data_phase_farmer[plant_index].phase.findIndex((e) => e.phase_id === phase_id)
            if (plant_index >= 0 && phase_index >= 0) {
                return_data = this.state.data_phase_farmer[plant_index].phase[phase_index]
            }
        }
        // console.log("return_data", plant_index)
        return return_data
    }

    handleOverflowChange(isOverflowed) {
        console.log(isOverflowed);
    }

    onChangeEdit = (event) => {

        let data = event
        // console.log(data)

        let data_str = JSON.stringify(this.state.data_phase_farmer)
        this.setState({
            edit: !this.state.edit,
            phase_id_check: event.phase_id,
            data_phase_farmer_cancel: data_str,
            data_update_database: data
        }
            // , () => { console.log("data", this.state.data_update_database) }
        )
    }

    onEditValue = (event, index, index_phase) => {
        let data = this.state.data_phase_farmer
        let data_phase = this.state.data_phase_farmer[index]
        data_phase.phase[index_phase].plant_value = event.target.value
        data[index] = data_phase
        this.setState({
            data_phase_farmer: data,
            data_update_database: data_phase.phase[index_phase]
        })
    }

    onEditDate = (event, index, index_phase) => {
        let data = this.state.data_phase_farmer
        let data_phase = this.state.data_phase_farmer[index]
        data_phase.phase[index_phase].havest_date = moment(event).format("DD-MM-YYYY")
        data[index] = data_phase
        this.setState({
            data_phase_farmer: data,
            data_update_database: data_phase.phase[index_phase]
        })
    }

    onEditSave = () => {
        this.setState({
            edit: false,
        }, () => { this.update_phase_data() })
    }

    onEditCancel = () => {
        this.setState({
            data_phase_farmer: JSON.parse(this.state.data_phase_farmer_cancel),
            edit: false
        })
        // console.log("cen", this.state.data_phase_farmer_cancel)
    }

    onDelete = (event, index, index_phase) => {

        let database_del = event
        console.log("data", database_del)
        swal({
            buttons: [true, "แน่ใจที่จะลบสเตจนี้?"],
        }); swal({
            title: "แน่ใจที่จะลบสเตจนี้?",
            text: "คุณต้องการลบสเตจนี้!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            console.log("alret : ", willDelete)
            if (willDelete) {
                swal("ลบเรียบร้อย!", {
                    icon: "success"
                });

                let data = this.state.data_phase_farmer
                let data_del = data[index].phase

                data_del.splice(index_phase, 1)
                console.log("data", data)

                this.setState({
                    data_phase_farmer: data,
                    data_del_database: database_del
                }, () => { this.delete_phase_data() })
            } else {
                swal("ยกเลิก!");
            }
        });
    }

    onSavetodata = async () => {

        let body = {
            data_phase_farmer: this.state.data_to_database
        }

        let return_st = false;
        console.log("body", body)
        try {
            await post(body, 'neo_firm/set_farmer_phase', user_token).then((result) => {
                if (result.success) {
                    return_st = true
                } else {
                    return_st = false
                }
            });
        } catch (error) {
            return_st = false
        }
        return return_st
    }

    delete_phase_data = async () => {

        let body = this.state.data_del_database

        let return_st = false;

        try {
            await post(body, 'neo_firm/delete_farmer_phase', user_token).then((result) => {
                if (result.success) {
                    return_st = true
                } else {
                    return_st = false
                }
            });
        } catch (error) {
            return_st = false
        }
        return return_st
    }

    update_phase_data = async () => {

        let body = this.state.data_update_database

        let return_st = false;

        try {
            await post(body, 'neo_firm/update_farmer_phase', user_token).then((result) => {
                if (result.success) {
                    return_st = true
                } else {
                    return_st = false
                }
            });
        } catch (error) {
            return_st = false
        }
        return return_st
    }

    render() {
        if (!this.props.location.params) {
            return <Redirect to="/S_state" />
        }

        const { data_farmer } = this.state
        return (
            <div className="App">
                <h2 style={{ textAlign: "center" }}>
                    <div>
                        ข้อมูลเฟสพืชของ {data_farmer.first_name} {data_farmer.last_name}
                    </div>
                </h2>
                <div className="Row">

                    <div className="col-2 "> </div>
                    {/* ฝั่งซ้าย */}
                    <div className="col-4 ">
                        <div></div>
                        <h3>ข้อมูลของเกษตรกร</h3>

                        <div className="Card_faemer">
                            <div className="Row">
                                <div className="col-1" />
                                {/* <div className="col-2"><img src={persor} className="img-persor" /></div> */}
                                <div className="col-8">
                                    <div style={{ textAlign: "left", fontSize: 30, color: "#ffffff", lineHeight: 0.8, marginLeft: 60 }}>
                                        <h4>ชื่อ : {data_farmer.title_name} {data_farmer.first_name}  {data_farmer.last_name} </h4>
                                        <h4>เบอร์โทรติดต่อ : {data_farmer.phone_number}</h4>
                                        <h4>ที่อยู่ : {data_farmer.address.detail} {data_farmer.address.district}   </h4>
                                        <h4>{data_farmer.address.district_tambons}   {data_farmer.address.postcode} {data_farmer.address.province}</h4>
                                    </div>
                                </div>
                                <div className="col-1" />
                            </div>
                        </div>

                        <div className="bg_date" >
                            <div className="Row">
                                <div className="col-4">
                                    <h4>เลือกพืชที่ต้องการ  : </h4>
                                    <h4>วันที่เริ่มเพาะปลูก  : </h4>
                                </div>

                                <div className="col-4">
                                    <div style={{ marginTop: 35 }} >
                                        <Combobox
                                            // initialSelectedItem={{ label: '' }}
                                            openOnFocus
                                            items={this.render_plant_array(this.state.data_plant)}
                                            itemToString={item => item ? item.label : ''}
                                            onChange={(event) => {
                                                this.setState({
                                                    combobox_obj: event
                                                }, () => {
                                                    // console.log("combobox_obj : ", this.state.combobox_obj)
                                                })
                                                this.set_data_phase(event)
                                            }}
                                        />
                                    </div>
                                    <div style={{ marginTop: 20 }} >
                                        <DatePicker
                                            dateFormat="dd/MM/yyyy"
                                            selected={moment(this.state.startDate, "YYYY/MM/DD").toDate()}
                                            onChange={(event) => { this.set_date(event) }}
                                        />
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div className="bg_overflow" style={{ marginTop: 20 }}>
                            <div className=" overflow_life" >
                                {this.state.phase_array.map((phase_array_element, index) => {
                                    // {console.log("this.state.phase_array : ",this.state.phase_array)}
                                    return <Set_phase data_phase_farmer={{
                                        ...phase_array_element,
                                        index: index,
                                        farmer_phase: this.farmer_phase_compare(phase_array_element.id, this.state.plant_id)
                                    }}
                                        set_date_phase_component={(event) => {
                                            this.set_date_phase_component(event, index)
                                        }}
                                        input_value_phase={(event) => {
                                            this.input_value_phase(event, index)
                                        }}
                                    />
                                })
                                }
                            </div>

                            {this.state.page ?
                                <div >
                                    {/* ปุ่ม */}
                                    <button className="btn-save"
                                        style={{
                                            marginTop: "5%",
                                            backgroundColor: "#ff9800",
                                            width: 200
                                        }}
                                        onClick={() => { this.onSave() }}>บันทึก</button>

                                </div>

                                : null}
                        </div>
                    </div>




                    {/* ฝั่งขวา */}
                    <div className="col-4 ">

                        <div className="wall">
                            <h3 style={{ textAlign: "center", marginTop: 20 }}>รายการเฟสของเกษตรกร</h3>
                            <div className="overflow_right">
                                {this.state.data_phase_farmer.map((element, index) => {
                                    return (
                                        <div>
                                            <div className="bg_list_phase" >
                                                <div style={{ fontSize: 24, color: "#ffffff" }}>
                                                    ชนิดพืช : {element.plant_name}
                                                </div>
                                                {element.phase.map((element_pha, index_phase) => {
                                                    return (
                                                        <div className="Card_faemer2">
                                                            {!this.state.edit ?
                                                                <div className="Row">
                                                                    {/* {console.log(this.state.edit)} */}
                                                                    <div className="col-1" />
                                                                    <div className="col-8">
                                                                        <div style={{ textAlign: "left", fontSize: 24, color: "#ffffff", marginTop: 10 }}>
                                                                            <div>เฟสพืช : {element_pha.phase_name}</div>
                                                                            <div>วันที่ส่งมอบ : {element_pha.havest_date}</div>
                                                                            <div>จำนวน : {element_pha.plant_value} (กก.)</div>
                                                                        </div>
                                                                    </div>
                                                                    <div />
                                                                    <div style={{marginTop:33,marginLeft:50}} >
                                                                        <button className="btn-card" onClick={() => { this.onChangeEdit(element_pha) }}>
                                                                            <img src={edit} className="img-card" />
                                                                        </button>
                                                                        <button className="btn-card" onClick={() => { this.onDelete(element_pha, index, index_phase) }}>
                                                                            <img src={close} className="img-card" />
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                                :
                                                                <>
                                                                    {this.state.phase_id_check === element_pha.phase_id ?
                                                                        <div className="Row">
                                                                            <div className="col-1" />
                                                                            <div className="col-8">
                                                                                <div style={{ textAlign: "left", fontSize: 24, color: "#ffffff" }}>
                                                                                    <div>เฟสพืช : {element_pha.phase_name}</div>
                                                                                    <div>วันที่ : <DatePicker
                                                                                        dateFormat="dd/MM/yyyy"
                                                                                        selected={moment(element_pha.havest_date, "DD/MM/YYYY").toDate()}
                                                                                        onChange={(event) => { this.onEditDate(event, index, index_phase) }}
                                                                                        style={{
                                                                                            height: 15,
                                                                                            marginTop: 20
                                                                                        }} />
                                                                                    </div>
                                                                                จำนวน : <input style={{ width: 50, height: 10, marginTop: 2 }} type="number"
                                                                                        value={element_pha.plant_value}
                                                                                        onChange={(event) => { this.onEditValue(event, index, index_phase) }} />
                                                                                </div>
                                                                            </div>
                                                                            {/* <button onClick={() => { this.onEditSave() }}>บันทึก</button>
                                                                            <button onClick={() => { this.onEditCancel() }}>ยกเลิก</button> */}
                                                                        </div>
                                                                        :
                                                                        <div className="Row">
                                                                            <div className="col-1" />
                                                                            <div className="col-8">
                                                                                <div style={{ textAlign: "left", fontSize: 24, color: "#ffffff" }}>
                                                                                    <div>เฟสพืช : {element_pha.phase_name}</div>
                                                                                    <div>วันที่ส่งมอบ : {element_pha.havest_date}</div>
                                                                                    <div>จำนวน : {element_pha.plant_value} (กก.)</div>
                                                                                </div>
                                                                            </div>
                                                                            <div />
                                                                        </div>}
                                                                    <div />
                                                                </>
                                                            }
                                                        </div>
                                                    )
                                                })}



                                            </div>
                                        </div>
                                    )
                                })}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        );
    }
}


export default S_state_show