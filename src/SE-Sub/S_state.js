import React, { Component } from 'react'
import { get, post, ip } from '../Support/Service'
import { user_token, sortData } from '../Support/Constance'
import { NavLink, Link } from 'react-router-dom'
import Modal from 'react-responsive-modal';
// import S_state_show from './S_state_show'

class S_state extends Component {

    constructor() {
        super();
        this.state = {
            data_farmer: [],
            data_phahe_farmer: [],
            search_order: []
        }
    }

    componentWillMount() {
        this.get_data_farmer()
    }

    get_data_farmer = async () => {
        try {
            await get('neo_firm/get_farmer', user_token).then((result) => {
                if (result.success) {
                    this.setState({
                        data_farmer: result.result,
                        search_order: result.result
                    })
                    // console.log('data_farmer : ', this.state.data_farmer)
                } else {
                }
            });
        } catch (error) {
            alert("get_data_farmer" + error);
        }
    }

    filterFarmer = (e) => {

        let name = e.target.value
        console.log("name : ",name)

        var updatedList = this.state.search_order;
        updatedList = updatedList.filter(function (item) {
            // console.log("item : ",item.first_name)
            return item.first_name.search(e.target.value) !== -1;
        });

        // console.log("updatedList : ",updatedList)
        this.setState({
            data_farmer: updatedList,
        });
    }

    render() {
        return (
            <div className="App">
                <h2 style={{ textAlign: "center" }}>ข้อมูลเกษตรกร</h2>
                <div className="Row">
                
                    <div className="col-2 "> </div>
                    <div className="col-8 ">
                                        <input type="text" placeholder="ค้นหาเกษตรกร"  onChange={this.filterFarmer} style={{ width: "98%" , marginBottom:10 }}/>
                        <table style={{ textAlign: "center" }}>
                            <tr >
                                <th>ลำดับ</th>
                                <th>ชื่อ - นามสกุล</th>
                                <th>ข้อมูล</th>
                            </tr>
                            {this.state.data_farmer.map((element, index) => {
                                return (
                                    <tr>
                                        <td>{index + 1}</td>
                                        <td>{element.first_name} {element.last_name}</td>
                                        <td>
                                            <NavLink to={{ pathname: "/S_state_show", params: { data: element } }} >
                                                <button  className = "btn_click"  > ดูข้อมูล</button>
                                            </NavLink>
                                        </td>
                                    </tr>
                                )
                            })}
                        </table> </div>

                    <div className="col-2 ">  </div>
                </div>


            </div>
        );
    }

}

export default S_state